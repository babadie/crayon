use crayon::canvas::Canvas;
use crayon::color::Color;
use crayon::light::PointLight;
use crayon::ray::{hit, Hittable, Ray};
use crayon::shapes::sphere::Sphere;
use crayon::shapes::Object;
use crayon::tuple::Tuple;

mod common;

use common::identical_ppm;

#[test]
fn test_ch06_sphere() {
    let size = 100;
    let mut canvas: Canvas = Canvas::new(size, size);

    // Our sphere is centered at origin
    let mut sphere = Sphere::simple();

    // And is purple
    sphere.shape.material.color = Color::new(1.0, 0.2, 1.0);

    // Rotate & tear the sphere
    /*
    sphere.set_transform(
        &(Matrix::shearing(1.0, 0.0, 0.0, 0.2, 0.0, 0.0)
            * Matrix::rotation_z(0.3)
            * Matrix::scaling(0.5, 1.2, 1.0)),
    );
    */

    // Use a point light source behind, above and to the left of the eye
    let light = PointLight::new(Color::white(), Tuple::point(-10.0, 10.0, -10.0));

    // All our ray start at the same position
    let origin = Tuple::point(0.0, 0.0, -5.0);

    // Wall behind is bigger
    // We need to convert from 3D coords to raster
    let wall = 5.0;
    let pixel_size = wall / (size as f64);

    for x in 0..size {
        let scene_x = -(wall / 2.0) + (x as f64) * pixel_size;

        for y in 0..size {
            let scene_y = (wall / 2.0) - (y as f64) * pixel_size;

            // We try to hit that target on the wall
            let target = Tuple::point(scene_x, scene_y, wall);

            // Shoot !
            let ray = Ray::new(origin, (target - origin).normalize());
            let xs = sphere.intersect(ray);

            // Calc the color by applying our light model
            // When an object is hit
            let color = match hit(xs) {
                None => Color::black(),
                Some(hit) => {
                    if let Object::Sphere(sphere) = hit.object {
                        let position = ray.position(hit.time);
                        let normal = sphere.normal_at(position);
                        let eye = -ray.direction;
                        light.apply(sphere.shape.material, position, eye, normal, false)
                    } else {
                        panic!("No sphere !");
                    }
                }
            };
            canvas.set_pixel(x, y, color);
        }
    }

    // Check output
    assert!(identical_ppm(canvas, "tests/images/sphere.ppm"));
}
