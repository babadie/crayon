use crayon::canvas::Canvas;
use crayon::color::Color;
use crayon::matrix::Matrix;
use crayon::tuple::Tuple;

mod common;

use common::identical_ppm;

// Render a PPM with some diagonals
#[test]
fn test_ch02_diagonals() {
    let mut c: Canvas = Canvas::new(100, 100);
    let red = Color::new(1.0, 0.0, 0.0);

    // Add diagonals
    for x in 0..c.width {
        for y in 0..c.height {
            if x == y {
                c.set_pixel(x, y, red);
                c.set_pixel(x, c.height - y - 1, red);
            }
        }
    }

    // Check output
    assert!(identical_ppm(c, "tests/images/diagonals.ppm"));
}

#[test]
fn test_ch03_matrices() {
    // Invert identity
    let id = Matrix::identity(4);
    assert_eq!(
        format!("ID = {}", id),
        "ID = Matrix 4x4:
1.00 0.00 0.00 0.00
0.00 1.00 0.00 0.00
0.00 0.00 1.00 0.00
0.00 0.00 0.00 1.00
"
    );
    let inv = id.inverse();
    assert_eq!(
        format!("ID * inverse(ID) = {}", inv),
        "ID * inverse(ID) = Matrix 4x4:
1.00 0.00 0.00 0.00
0.00 1.00 0.00 0.00
0.00 0.00 1.00 0.00
0.00 0.00 0.00 1.00
"
    );

    // Multiply by inverse
    let a = Matrix::new(vec![
        vec![1.0, 2.0, 3.0],
        vec![4.0, 5.0, -6.0],
        vec![7.0, 8.0, 9.0],
    ]);
    assert_eq!(
        format!("A = {}", a),
        "A = Matrix 3x3:
1.00 2.00 3.00
4.00 5.00 -6.00
7.00 8.00 9.00
"
    );
    let inv = a.inverse();
    assert_eq!(
        format!("A * inverse(A) = {}", a.clone() * inv),
        "A * inverse(A) = Matrix 3x3:
1.00 0.00 0.00
0.00 1.00 0.00
0.00 0.00 1.00
"
    );

    // Transpose + inverse are associative
    let b = a.inverse().transpose();
    assert_eq!(
        format!("transpose(inverse(A)) = {}", b),
        "transpose(inverse(A)) = Matrix 3x3:
-1.29 1.08 0.04
-0.08 0.17 -0.08
0.38 -0.25 0.04
"
    );
    let c = a.transpose().inverse();
    assert_eq!(
        format!("inverse(transpose(A)) = {}", c),
        "inverse(transpose(A)) = Matrix 3x3:
-1.29 1.08 0.04
-0.08 0.17 -0.08
0.38 -0.25 0.04
"
    );
}

#[test]
fn test_ch04_clock() {
    let size = 400;
    let mut c: Canvas = Canvas::new(size, size);
    let red = Color::new(1.0, 0.0, 0.0);

    // Add top point at twelve o clock
    let orig = Tuple::point(0.0, (size as f64) / 3.0, 0.0);
    let offset = (size as f64) / 2.0;

    // Move point with a rotation matrix affecting x & y only
    for i in 0..12 {
        let m = Matrix::rotation_z((i as f64) * std::f64::consts::PI / 6.0);
        let p = m * orig;

        let x = (offset + p.x) as usize;
        let y = (offset + p.y) as usize;
        println!("> {:?} => ({}, {})", p, x, y);
        c.set_pixel(x, y, red);
    }

    // Check output
    assert!(identical_ppm(c, "tests/images/clock.ppm"));
}
