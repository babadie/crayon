use crayon::canvas::Canvas;
use std::fs::File;
use std::io::prelude::*;

// Check that a Canvas current data render exactly like an existing PPM file
pub fn identical_ppm(canvas: Canvas, path: &str) -> bool {
    // Load comparison PPM
    let mut file = File::open(path).expect("Missing comparison file");
    let mut valid_ppm = String::new();
    file.read_to_string(&mut valid_ppm)
        .expect("Failed to load comparison file");

    // Check valid file with canvas' render
    return valid_ppm == canvas.build_ppm();
}
