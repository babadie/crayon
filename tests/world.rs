use crayon::color::Color;
use crayon::light::PointLight;
use crayon::matrix::Matrix;
use crayon::scene::{Camera, World};
use crayon::shapes::plane::Plane;
use crayon::shapes::sphere::Sphere;
use crayon::shapes::Object;
use crayon::tuple::Tuple;

mod common;

use common::identical_ppm;

#[test]
fn test_ch07_world() {
    // Start with an empty world
    let mut world = World::new();

    // Setup a light source, shining from above and to the left
    world.add_light(PointLight::new(
        Color::white(),
        Tuple::point(-10.0, 10.0, -10.0),
    ));

    // The floor is an extremely flattened sphere
    let mut floor = Sphere::simple();
    floor.shape.transform = Matrix::scaling(10.0, 0.01, 10.0);
    floor.shape.material.color = Color::new(1.0, 0.9, 0.0);
    floor.shape.material.specular = 0.0;
    world.add_object(Object::from(floor));

    // Left wall is the same as floor but rotated and translated
    let mut left_wall = Sphere::simple();
    left_wall.shape.transform = Matrix::translation(0.0, 0.0, 5.0)
        * Matrix::rotation_y(-std::f64::consts::PI / 4.0)
        * Matrix::rotation_x(-std::f64::consts::PI / 2.0)
        * Matrix::scaling(10.0, 0.01, 10.0);
    left_wall.shape.material.color = Color::new(1.0, 0.9, 0.0);
    left_wall.shape.material.specular = 0.0;
    world.add_object(Object::from(left_wall));

    // Right wall is the same as floor but rotated and translated
    // Opposite from left wall
    let mut right_wall = Sphere::simple();
    right_wall.shape.transform = Matrix::translation(0.0, 0.0, 5.0)
        * Matrix::rotation_y(std::f64::consts::PI / 4.0)
        * Matrix::rotation_x(std::f64::consts::PI / 2.0)
        * Matrix::scaling(10.0, 0.01, 10.0);
    right_wall.shape.material.color = Color::new(1.0, 0.9, 0.0);
    right_wall.shape.material.specular = 0.0;
    world.add_object(Object::from(right_wall));

    // Middle shpere is a green unit sphere, translated upward
    let mut middle = Sphere::simple();
    middle.shape.transform = Matrix::translation(-0.5, 1.0, 0.5);
    middle.shape.material.color = Color::new(0.1, 1.0, 0.5);
    middle.shape.material.diffuse = 0.7;
    middle.shape.material.specular = 0.3;
    world.add_object(Object::from(middle));

    // The smaller green one is positionned on the right and scaled in half
    let mut right = Sphere::simple();
    right.shape.transform = Matrix::translation(1.5, 0.5, -0.5) * Matrix::scaling(0.5, 0.5, 0.5);
    right.shape.material.color = Color::new(0.5, 1.0, 0.1);
    right.shape.material.diffuse = 0.7;
    right.shape.material.specular = 0.3;
    world.add_object(Object::from(right));

    // The smallest one is scaled by a third and translated
    let mut left = Sphere::simple();
    left.shape.transform =
        Matrix::translation(-1.5, 0.33, -0.75) * Matrix::scaling(0.33, 0.33, 0.33);
    left.shape.material.color = Color::new(1.0, 0.8, 0.1);
    left.shape.material.diffuse = 0.7;
    left.shape.material.specular = 0.3;
    world.add_object(Object::from(left));

    // Setup camera to render
    let mut camera = Camera::new(100, 50, std::f64::consts::PI / 3.0);
    camera.transform = Matrix::view_transform(
        Tuple::point(0.0, 1.5, -5.0),
        Tuple::point(0.0, 1.0, 0.0),
        Tuple::vector(0.0, 1.0, 0.0),
    );
    let canvas = camera.render(world);

    // Check the output
    assert!(identical_ppm(canvas, "tests/images/ch08.ppm"));
}

#[test]
fn test_ch09_plane() {
    // Start with an empty world
    let mut world = World::new();

    // Setup a light source, shining from above and to the left
    world.add_light(PointLight::new(
        Color::white(),
        Tuple::point(-10.0, 10.0, -10.0),
    ));

    // The floor is a yellow plane
    let mut floor = Plane::new();
    floor.shape.material.color = Color::new(1.0, 0.9, 0.0);
    floor.shape.material.diffuse = 0.7;
    floor.shape.material.specular = 0.5;
    world.add_object(Object::from(floor));

    // The sky is a blueish plane
    let mut sky = Plane::new();
    sky.shape.transform = Matrix::translation(0.0, 12.0, 0.0);
    sky.shape.material.color = Color::new(0.0, 0.2, 1.0);
    sky.shape.material.diffuse = 1.0;
    sky.shape.material.specular = 0.8;
    world.add_object(Object::from(sky));

    // The wall is a red plane
    let mut wall = Plane::new();
    wall.shape.transform = Matrix::translation(0.0, 0.0, 40.0)
        * Matrix::rotation_x(std::f64::consts::PI / 2.0)
        * Matrix::rotation_z(0.4);
    wall.shape.material.color = Color::new(1.0, 0.0, 0.1);
    wall.shape.material.diffuse = 0.5;
    wall.shape.material.specular = 0.4;
    world.add_object(Object::from(wall));

    // Middle shpere is a green unit sphere, translated upward
    let mut middle = Sphere::simple();
    middle.shape.transform = Matrix::translation(-0.3, 0.2, 0.0);
    middle.shape.material.color = Color::new(0.1, 1.0, 0.5);
    middle.shape.material.diffuse = 0.7;
    middle.shape.material.specular = 0.3;
    world.add_object(Object::from(middle));

    // The smaller green one is positionned on the right and scaled in half
    let mut right = Sphere::simple();
    right.shape.transform = Matrix::translation(1.5, 0.5, -0.5) * Matrix::scaling(0.5, 0.5, 0.5);
    right.shape.material.color = Color::new(0.5, 1.0, 0.1);
    right.shape.material.diffuse = 0.7;
    right.shape.material.specular = 0.3;
    world.add_object(Object::from(right));

    // The smallest one is scaled by a third and translated
    let mut left = Sphere::simple();
    left.shape.transform =
        Matrix::translation(-1.8, 0.33, -0.75) * Matrix::scaling(0.33, 0.33, 0.33);
    left.shape.material.color = Color::new(1.0, 0.8, 0.1);
    left.shape.material.diffuse = 0.7;
    left.shape.material.specular = 0.3;
    world.add_object(Object::from(left));

    // Setup camera to render
    let mut camera = Camera::new(100, 50, std::f64::consts::PI / 3.0);
    camera.transform = Matrix::view_transform(
        Tuple::point(0.3, 0.8, -5.0),
        Tuple::point(0.0, 1.0, 0.0),
        Tuple::vector(0.0, 1.0, 0.0),
    );
    let canvas = camera.render(world);

    // Check the output
    assert!(identical_ppm(canvas, "tests/images/ch09.ppm"));
}
