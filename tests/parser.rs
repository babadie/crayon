use crayon::color::Color;
use crayon::light::PointLight;
use crayon::matrix::Matrix;
use crayon::parser::parse_scene;
use crayon::scene::{Camera, World};
use crayon::shapes::plane::Plane;
use crayon::shapes::sphere::Sphere;
use crayon::shapes::Object;
use crayon::tuple::Tuple;

mod common;

#[test]
fn test_yaml_simple() {
    let (camera, world) = parse_scene("tests/scenes/simple.yml");

    // Check the camera values
    let mut expected_camera = Camera::new(1920, 1080, 1.8);
    expected_camera.transform = Matrix::view_transform(
        Tuple::point(0.0, 1.5, -5.0),
        Tuple::point(0.0, 1.0, 0.0),
        Tuple::vector(0.0, 1.0, 0.0),
    );
    assert_eq!(camera, expected_camera);

    // Check the world values
    let mut expected_world = World::new();
    expected_world.add_light(PointLight::new(
        Color::new(1.0, 0.0, 0.0),
        Tuple::point(-10.0, 10.0, -10.0),
    ));
    expected_world.add_object(Object::from(Sphere::simple()));
    expected_world.add_object(Object::from(Plane::new()));
    assert_eq!(world, expected_world);
}

#[test]
fn test_yaml_default_world() {
    let (camera, world) = parse_scene("tests/scenes/default.yml");
    assert_eq!(camera, Camera::new(200, 100, 1.2));
    assert_eq!(world, World::default());
}
