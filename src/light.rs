use crate::color::Color;
use crate::parser::{deserialize_color, deserialize_point};
use crate::tuple::Tuple;
use serde;
use serde::Deserialize;

#[derive(Debug, Copy, Clone, Deserialize)]
pub struct PointLight {
    #[serde(rename = "color")]
    #[serde(deserialize_with = "deserialize_color")]
    intensity: Color,

    #[serde(deserialize_with = "deserialize_point")]
    pub position: Tuple,
}

impl PointLight {
    pub fn new(intensity: Color, position: Tuple) -> Self {
        assert!(position.is_point());
        Self {
            intensity: intensity,
            position: position,
        }
    }

    // Calc the color output from that light on a Material at given position
    // for a specific view (eye) & reflection
    // This is really the Phong reflection implementation
    pub fn apply(
        &self,
        material: Material,
        position: Tuple,
        eyev: Tuple,
        normalv: Tuple,
        in_shadow: bool,
    ) -> Color {
        assert!(position.is_point());
        assert!(eyev.is_vector());
        assert!(normalv.is_vector());

        // Combine surface color with light's intensity
        let effective_color = material.color * self.intensity;

        // Find the direction to the light source
        let lightv = (self.position - position).normalize();
        assert!(lightv.is_vector());

        // Compute the ambient contribution
        let ambient = effective_color * material.ambient;

        // Only use the ambient light source when in shadows
        if in_shadow {
            return ambient;
        }

        // This value is the cosine of the angle between the light vector
        // and the normal vector
        let light_dot_normal = lightv.dot(normalv);

        // When it's negative, the light is on the other side of the surface
        // So we only use the ambient light source
        if light_dot_normal < 0.0 {
            return ambient;
        }

        // If we are on the exposed side, we need to calc the diffuse and specular parts
        let diffuse = effective_color * material.diffuse * light_dot_normal;

        // This value is the cosine of the angle between the reflection vector
        // and the eye vector
        let reflectv = (-lightv).reflect(normalv);
        let reflect_dot_eye = reflectv.dot(eyev);

        // Compute the specular source
        // A negative reflect_dot_eye means the light reflects away from the eye
        let specular = if reflect_dot_eye <= 0.0 {
            Color::black()
        } else {
            let factor = reflect_dot_eye.powf(material.shininess);
            self.intensity * material.specular * factor
        };

        // Finally combine the three sources together
        return ambient + diffuse + specular;
    }
}

impl PartialEq for PointLight {
    fn eq(&self, other: &Self) -> bool {
        return self.intensity == other.intensity && self.position == other.position;
    }
}

fn default_ambient() -> f64 {
    0.1
}
fn default_diffuse() -> f64 {
    0.9
}
fn default_specular() -> f64 {
    0.9
}
fn default_shininess() -> f64 {
    200.0
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct Material {
    #[serde(deserialize_with = "deserialize_color")]
    pub color: Color,

    #[serde(default = "default_ambient")]
    pub ambient: f64,

    #[serde(default = "default_diffuse")]
    pub diffuse: f64,

    #[serde(default = "default_specular")]
    pub specular: f64,

    #[serde(default = "default_shininess")]
    pub shininess: f64,
}

impl Default for Material {
    // Default material to use for Phong reflection
    fn default() -> Self {
        Self {
            color: Color::white(),
            ambient: default_ambient(),
            diffuse: default_diffuse(),
            specular: default_specular(),
            shininess: default_shininess(),
        }
    }
}

impl PartialEq for Material {
    fn eq(&self, other: &Self) -> bool {
        return self.color == other.color
            && self.ambient == other.ambient
            && self.diffuse == other.diffuse
            && self.specular == other.specular
            && self.shininess == other.shininess;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // A point light has a position and intensity
    #[test]
    fn test_light_init() {
        let intensity = Color::new(1.0, 1.0, 1.0);
        let position = Tuple::point(0.0, 0.0, 0.0);
        let light = PointLight::new(intensity, position);
        assert_eq!(light.intensity, intensity);
        assert_eq!(light.position, position);
    }

    // The default material
    #[test]
    fn test_material_default() {
        let m = Material::default();
        assert_eq!(m.color, Color::new(1.0, 1.0, 1.0));
        assert_eq!(m.ambient, 0.1);
        assert_eq!(m.diffuse, 0.9);
        assert_eq!(m.specular, 0.9);
        assert_eq!(m.shininess, 200.0);
    }

    // Lightning with the eye between the light & surface
    #[test]
    fn test_lightning_1() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, 0.0, -1.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, -10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, false),
            Color::new(1.9, 1.9, 1.9)
        );
    }

    // Lightning with the eye between the light & surface, eye offset 45°
    #[test]
    fn test_lightning_2() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, (2.0 as f64).sqrt() / 2.0, (2.0 as f64).sqrt() / 2.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, -10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, false),
            Color::new(1.0, 1.0, 1.0)
        );
    }

    // Lightning with the eye opposite surface, light offset 45°
    #[test]
    fn test_lightning_3() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, 0.0, -1.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 10.0, -10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, false),
            Color::new(0.7364, 0.7364, 0.7364)
        );
    }

    // Lightning with eye in the path of the reflection vector
    #[test]
    fn test_lightning_4() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, -(2.0 as f64).sqrt() / 2.0, -(2.0 as f64).sqrt() / 2.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 10.0, -10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, false),
            Color::new(1.6364, 1.6364, 1.6364)
        );
    }

    // Lightning with the light behind the surface
    #[test]
    fn test_lightning_5() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, 0.0, -1.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, 10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, false),
            Color::new(0.1, 0.1, 0.1)
        );
    }

    // Lightning with the surface in shadow
    #[test]
    fn test_shadows() {
        let m = Material::default();
        let position = Tuple::point(0.0, 0.0, 0.0);
        let eyev = Tuple::vector(0.0, 0.0, -1.0);
        let normalv = Tuple::vector(0.0, 0.0, -1.0);
        let light = PointLight::new(Color::new(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, -10.0));
        assert_eq!(
            light.apply(m, position, eyev, normalv, true),
            Color::new(0.1, 0.1, 0.1)
        );
    }
}
