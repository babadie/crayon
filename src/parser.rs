use crate::color::Color;
use crate::scene::{Camera, World};
use serde_yaml;
use std::fs::File;
use std::path::Path;

use crate::matrix::Matrix;
use crate::tuple::Tuple;
use serde;
use serde::de::Error;
use serde::{Deserialize, Deserializer};

use serde_yaml::Value;

pub fn deserialize_point<'de, D>(d: D) -> Result<Tuple, D::Error>
where
    D: Deserializer<'de>,
{
    let value = Value::deserialize(d)?;
    assert!(value.is_sequence());
    let seq = value.as_sequence().unwrap();
    assert_eq!(seq.len(), 3);
    Ok(Tuple::point(
        seq[0].as_f64().unwrap(),
        seq[1].as_f64().unwrap(),
        seq[2].as_f64().unwrap(),
    ))
}

pub fn deserialize_color<'de, D>(d: D) -> Result<Color, D::Error>
where
    D: Deserializer<'de>,
{
    let value = Value::deserialize(d)?;
    if value.is_string() {
        // Support known colors as strings
        match value.as_str() {
            Some("white") => Ok(Color::white()),
            Some("black") => Ok(Color::black()),
            Some("red") => Ok(Color::new(1.0, 0.0, 0.0)),
            Some("green") => Ok(Color::new(0.0, 1.0, 0.0)),
            Some("blue") => Ok(Color::new(0.0, 0.0, 1.0)),
            _ => Err(D::Error::custom(format!(
                "Unknown color {}",
                value.as_str().unwrap()
            ))),
        }
    } else if value.is_sequence() {
        // Support any color as sequence of floats
        let seq = value.as_sequence().unwrap();
        assert_eq!(seq.len(), 3);
        Ok(Color::new(
            seq[0].as_f64().unwrap(),
            seq[1].as_f64().unwrap(),
            seq[2].as_f64().unwrap(),
        ))
    } else {
        Err(D::Error::custom("Unsupported color syntax"))
    }
}

// Convert a list of Matrix transformations into a single Matrix
#[derive(Deserialize, Debug)]
pub struct Transformations(Vec<Transformation>);

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
enum Transformation {
    Translation(f64, f64, f64),
    Scaling(f64, f64, f64),
    RotationX(f64),
    RotationY(f64),
    RotationZ(f64),
    Shearing(f64, f64, f64, f64, f64, f64),
    ViewTransform([f64; 3], [f64; 3], [f64; 3]),
}

impl From<Transformations> for Matrix {
    // This is where all the transformations are chained together
    fn from(transformations: Transformations) -> Self {
        let mut out = Matrix::default();

        // The vector is encapsulated into a useless level of struct
        match transformations {
            Transformations(transforms) => {
                for tr in transforms {
                    // Convert each transformation declaration into a matrix
                    let m: Matrix = match tr {
                        Transformation::Translation(x, y, z) => Matrix::translation(x, y, z),
                        Transformation::Scaling(x, y, z) => Matrix::scaling(x, y, z),
                        Transformation::RotationX(angle) => Matrix::rotation_x(angle),
                        Transformation::RotationY(angle) => Matrix::rotation_y(angle),
                        Transformation::RotationZ(angle) => Matrix::rotation_z(angle),
                        Transformation::Shearing(xy, xz, yx, yz, zx, zy) => {
                            Matrix::shearing(xy, xz, yx, yz, zx, zy)
                        }
                        Transformation::ViewTransform(from, to, up) => Matrix::view_transform(
                            Tuple::point_from_array(from),
                            Tuple::point_from_array(to),
                            Tuple::vector_from_array(up),
                        ),
                    };

                    // And apply it to the overall output
                    out = out * m;
                }
            }
        }

        return out;
    }
}

#[derive(Deserialize, Debug)]
struct Conf {
    camera: Camera,

    // Magic trick from serde: the world's fields
    // are directly deserialized at this level
    #[serde(flatten)]
    world: World,
}

// Parse a YAML file describing a camera and world
pub fn parse_scene(path: &str) -> (Camera, World) {
    let path = Path::new(path);
    let display = path.display();

    // Parse YAML configuration into world and camera objects
    let conf: Conf = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => serde_yaml::from_reader(file).unwrap(),
    };

    return (conf.camera, conf.world);
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::de::value::{
        Error as ValueError, F64Deserializer, SeqDeserializer, StrDeserializer,
    };
    use serde::de::IntoDeserializer;

    // Check color deserialization from strings
    #[test]
    fn test_color_white() {
        let deserializer: StrDeserializer<ValueError> = "white".into_deserializer();
        assert_eq!(deserialize_color(deserializer), Ok(Color::white()));
    }

    #[test]
    fn test_color_black() {
        let deserializer: StrDeserializer<ValueError> = "black".into_deserializer();
        assert_eq!(deserialize_color(deserializer), Ok(Color::black()));
    }
    #[test]
    fn test_color_red() {
        let deserializer: StrDeserializer<ValueError> = "red".into_deserializer();
        assert_eq!(
            deserialize_color(deserializer),
            Ok(Color::new(1.0, 0.0, 0.0))
        );
    }
    #[test]
    fn test_color_green() {
        let deserializer: StrDeserializer<ValueError> = "green".into_deserializer();
        assert_eq!(
            deserialize_color(deserializer),
            Ok(Color::new(0.0, 1.0, 0.0))
        );
    }
    #[test]
    fn test_color_blue() {
        let deserializer: StrDeserializer<ValueError> = "blue".into_deserializer();
        assert_eq!(
            deserialize_color(deserializer),
            Ok(Color::new(0.0, 0.0, 1.0))
        );
    }
    #[test]
    fn test_color_unsupported() {
        let deserializer: StrDeserializer<ValueError> = "purple".into_deserializer();
        assert_eq!(
            deserialize_color(deserializer),
            Err(ValueError::custom("Unknown color purple"))
        );
    }
}
