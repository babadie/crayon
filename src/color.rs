use crate::utils::almost_eq;
use std::ops::{Add, Mul, Sub};

fn float_to_ppm(value: f64) -> String {
    // Convert a float value (should be between 0.0 & 1.0) to an integer between 0-255
    let limited: f64 = if value < 0.0 {
        0.0
    } else if value > 1.0 {
        1.0
    } else {
        value
    };
    format!("{}", (limited * 255.0).ceil())
}

#[derive(Debug, Copy, Clone)]
pub struct Color {
    red: f64,
    green: f64,
    blue: f64,
}

impl Color {
    pub fn new(red: f64, green: f64, blue: f64) -> Self {
        Self {
            red: red,
            green: green,
            blue: blue,
        }
    }

    pub fn black() -> Self {
        Self::new(0.0, 0.0, 0.0)
    }

    pub fn white() -> Self {
        Self::new(1.0, 1.0, 1.0)
    }

    pub fn to_ppm(self) -> Vec<String> {
        vec![
            float_to_ppm(self.red),
            float_to_ppm(self.green),
            float_to_ppm(self.blue),
        ]
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        almost_eq(self.red, other.red)
            && almost_eq(self.green, other.green)
            && almost_eq(self.blue, other.blue)
    }
}

impl Add for Color {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            red: self.red + other.red,
            green: self.green + other.green,
            blue: self.blue + other.blue,
        }
    }
}

impl Sub for Color {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            red: self.red - other.red,
            green: self.green - other.green,
            blue: self.blue - other.blue,
        }
    }
}

impl Mul for Color {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            red: self.red * other.red,
            green: self.green * other.green,
            blue: self.blue * other.blue,
        }
    }
}

impl Mul<f64> for Color {
    type Output = Self;

    fn mul(self, other: f64) -> Self {
        Self {
            red: self.red * other,
            green: self.green * other,
            blue: self.blue * other,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Colors are (red, green, blue) tuples
    #[test]
    fn test_init() {
        let c: Color = Color::new(-0.5, 0.4, 1.7);
        assert_eq!(c.red, -0.5);
        assert_eq!(c.green, 0.4);
        assert_eq!(c.blue, 1.7);
    }

    // Adding colors
    #[test]
    fn test_add() {
        let a: Color = Color::new(0.9, 0.6, 0.75);
        let b: Color = Color::new(0.7, 0.1, 0.25);
        assert_eq!(a + b, Color::new(1.6, 0.7, 1.0));
    }

    // Subtracting colors
    #[test]
    fn test_sub() {
        let a: Color = Color::new(0.9, 0.6, 0.75);
        let b: Color = Color::new(0.7, 0.1, 0.25);
        assert_eq!(a - b, Color::new(0.2, 0.5, 0.5));
    }

    // Multiplying a color by a scalar
    #[test]
    fn test_mul_scalar() {
        let a: Color = Color::new(0.2, 0.3, 0.4);
        assert_eq!(a * 2.0, Color::new(0.4, 0.6, 0.8));
    }

    // Multiplying colors
    #[test]
    fn test_mul() {
        let a: Color = Color::new(1.0, 0.2, 0.4);
        let b: Color = Color::new(0.9, 1.0, 0.1);
        assert_eq!(a * b, Color::new(0.9, 0.2, 0.04));
    }
}
