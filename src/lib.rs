pub const EPSILON: f64 = 0.0001;

pub mod canvas;
pub mod color;
pub mod light;
pub mod matrix;
pub mod parser;
pub mod ray;
pub mod scene;
pub mod shapes;
pub mod transformations;
pub mod tuple;
pub mod utils;
