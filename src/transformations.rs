use crate::matrix::Matrix;
use crate::tuple::Tuple;
use std::f64;

impl Matrix {
    /*
    Translation matrix:
    1 0 0 x
    0 1 0 y
    0 0 1 z
    0 0 0 1
    */
    pub fn translation(x: f64, y: f64, z: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(0, 3, x);
        t.set(1, 3, y);
        t.set(2, 3, z);
        return t;
    }

    /*
    Scaling matrix:
    x 0 0 0
    0 y 0 0
    0 0 z 0
    0 0 0 1
    */
    pub fn scaling(x: f64, y: f64, z: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(0, 0, x);
        t.set(1, 1, y);
        t.set(2, 2, z);
        return t;
    }

    /*
    Rotation matrix on X axis:
    1   0      0     0
    0 cos(r) -sin(r) 0
    0 sin(r)  cos(r) 0
    0   0      0     1
    */
    pub fn rotation_x(angle: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(1, 1, angle.cos());
        t.set(1, 2, -angle.sin());
        t.set(2, 1, angle.sin());
        t.set(2, 2, angle.cos());
        return t;
    }

    /*
    Rotation matrix on Y axis:
     cos(r) 0 sin(r) 0
       0    1   0    0
    -sin(r) 0 cos(r) 0
       0    0   0    1
    */
    pub fn rotation_y(angle: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(0, 0, angle.cos());
        t.set(0, 2, angle.sin());
        t.set(2, 0, -angle.sin());
        t.set(2, 2, angle.cos());
        return t;
    }

    /*
    Rotation matrix on Z axis:
    cos(r) -sin(r) 0 0
    sin(r)  cos(r) 0 0
      0       0    1 0
      0       0    0 1
    */
    pub fn rotation_z(angle: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(0, 0, angle.cos());
        t.set(0, 1, -angle.sin());
        t.set(1, 0, angle.sin());
        t.set(1, 1, angle.cos());
        return t;
    }

    /*
    Shearing across axes
    1  xy xz 0
    yx 1  yz 0
    zx zy 1  0
    0  0  0  1
    */
    pub fn shearing(xy: f64, xz: f64, yx: f64, yz: f64, zx: f64, zy: f64) -> Self {
        let mut t = Matrix::identity(4);
        t.set(0, 1, xy);
        t.set(0, 2, xz);
        t.set(1, 0, yx);
        t.set(1, 2, yz);
        t.set(2, 0, zx);
        t.set(2, 1, zy);
        return t;
    }

    // Helper to create a transformation matrix to setup the camera
    // The world moves, not the camera !
    pub fn view_transform(from: Tuple, to: Tuple, up: Tuple) -> Matrix {
        assert!(from.is_point());
        assert!(to.is_point());
        assert!(up.is_vector());

        let forward = (to - from).normalize();
        assert!(forward.is_vector());

        let left = forward.cross(up.normalize());
        assert!(left.is_vector());

        // This is a neat trick to avoid knowing the exact up direction
        let true_up = left.cross(forward);
        assert!(true_up.is_vector());

        let orientation = Matrix::new(vec![
            vec![left.x, left.y, left.z, 0.0],
            vec![true_up.x, true_up.y, true_up.z, 0.0],
            vec![-forward.x, -forward.y, -forward.z, 0.0],
            vec![0.0, 0.0, 0.0, 1.0],
        ]);

        return orientation * Matrix::translation(-from.x, -from.y, -from.z);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tuple::Tuple;

    // Multiplying by a translation matrix
    #[test]
    fn test_mul_translation() {
        let transform = Matrix::translation(5.0, -3.0, 2.0);
        let p = Tuple::point(-3.0, 4.0, 5.0);
        assert_eq!(transform * p, Tuple::point(2.0, 1.0, 7.0));
    }

    // Multiplying by the inverse of a translation matrix
    #[test]
    fn test_mul_inv_translation() {
        let transform = Matrix::translation(5.0, -3.0, 2.0);
        let inv = transform.inverse();
        let p = Tuple::point(-3.0, 4.0, 5.0);
        assert_eq!(inv * p, Tuple::point(-8.0, 7.0, 3.0));
    }

    // Translation does not affect vectors
    #[test]
    fn test_mul_vector_translation() {
        let transform = Matrix::translation(5.0, -3.0, 2.0);
        let v = Tuple::vector(-3.0, 4.0, 5.0);
        assert_eq!(transform * v, v);
    }

    //A scaling matrix applied to a point
    #[test]
    fn test_scaling_point() {
        let transform = Matrix::scaling(2.0, 3.0, 4.0);
        let p = Tuple::point(-4.0, 6.0, 8.0);
        assert_eq!(transform * p, Tuple::point(-8.0, 18.0, 32.0));
    }

    //A scaling matrix applied to a vector
    #[test]
    fn test_scaling_vector() {
        let transform = Matrix::scaling(2.0, 3.0, 4.0);
        let v = Tuple::vector(-4.0, 6.0, 8.0);
        assert_eq!(transform * v, Tuple::vector(-8.0, 18.0, 32.0));
    }

    //Multiplying by the inverse of a scaling matrix
    #[test]
    fn test_scaling_inverse() {
        let transform = Matrix::scaling(2.0, 3.0, 4.0);
        let inv = transform.inverse();
        let v = Tuple::vector(-4.0, 6.0, 8.0);
        assert_eq!(inv * v, Tuple::vector(-2.0, 2.0, 2.0));
    }

    //Reflection is scaling by a negative value
    #[test]
    fn test_refection() {
        let transform = Matrix::scaling(-1.0, 1.0, 1.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(-2.0, 3.0, 4.0));
    }

    //Rotating a point around the x axis
    #[test]
    fn test_rotate_point_x() {
        let p = Tuple::point(0.0, 1.0, 0.0);
        let half_quarter = Matrix::rotation_x(f64::consts::PI / 4.0);
        let full_quarter = Matrix::rotation_x(f64::consts::PI / 2.0);
        assert_eq!(
            half_quarter * p,
            Tuple::point(0.0, (2.0 as f64).sqrt() / 2.0, (2.0 as f64).sqrt() / 2.0)
        );
        assert_eq!(full_quarter * p, Tuple::point(0.0, 0.0, 1.0));
    }

    //The inverse of an x-rotation rotates in the opposite direction
    #[test]
    fn test_rotate_point_x_inverse() {
        let p = Tuple::point(0.0, 1.0, 0.0);
        let half_quarter = Matrix::rotation_x(f64::consts::PI / 4.0);
        let inv = half_quarter.inverse();
        assert_eq!(
            inv * p,
            Tuple::point(0.0, (2.0 as f64).sqrt() / 2.0, -(2.0 as f64).sqrt() / 2.0)
        );
    }

    //Rotating a point around the y axis
    #[test]
    fn test_rotate_point_y() {
        let p = Tuple::point(0.0, 0.0, 1.0);
        let half_quarter = Matrix::rotation_y(f64::consts::PI / 4.0);
        let full_quarter = Matrix::rotation_y(f64::consts::PI / 2.0);
        assert_eq!(
            half_quarter * p,
            Tuple::point((2.0 as f64).sqrt() / 2.0, 0.0, (2.0 as f64).sqrt() / 2.0)
        );
        assert_eq!(full_quarter * p, Tuple::point(1.0, 0.0, 0.0));
    }

    //Rotating a point around the z axis
    #[test]
    fn test_rotate_point_z() {
        let p = Tuple::point(0.0, 1.0, 0.0);
        let half_quarter = Matrix::rotation_z(f64::consts::PI / 4.0);
        let full_quarter = Matrix::rotation_z(f64::consts::PI / 2.0);
        assert_eq!(
            half_quarter * p,
            Tuple::point(-(2.0 as f64).sqrt() / 2.0, (2.0 as f64).sqrt() / 2.0, 0.0)
        );
        assert_eq!(full_quarter * p, Tuple::point(-1.0, 0.0, 0.0));
    }

    //A shearing transformation moves x in proportion to y
    #[test]
    fn test_shearing_x_y() {
        let transform = Matrix::shearing(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(5.0, 3.0, 4.0));
    }

    //A shearing transformation moves x in proportion to z
    #[test]
    fn test_shearing_x_z() {
        let transform = Matrix::shearing(0.0, 1.0, 0.0, 0.0, 0.0, 0.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(6.0, 3.0, 4.0));
    }

    //A shearing transformation moves y in proportion to x
    #[test]
    fn test_shearing_y_x() {
        let transform = Matrix::shearing(0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(2.0, 5.0, 4.0));
    }

    //A shearing transformation moves y in proportion to z
    #[test]
    fn test_shearing_y_z() {
        let transform = Matrix::shearing(0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(2.0, 7.0, 4.0));
    }

    //A shearing transformation moves z in proportion to x
    #[test]
    fn test_shearing_z_x() {
        let transform = Matrix::shearing(0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(2.0, 3.0, 6.0));
    }

    //A shearing transformation moves z in proportion to y
    #[test]
    fn test_shearing_z_y() {
        let transform = Matrix::shearing(0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
        let p = Tuple::point(2.0, 3.0, 4.0);
        assert_eq!(transform * p, Tuple::point(2.0, 3.0, 7.0));
    }

    // Individual transformations are applied in sequence
    #[test]
    fn test_sequence_individual() {
        let p = Tuple::point(1.0, 0.0, 1.0);
        let a = Matrix::rotation_x(f64::consts::PI / 2.0);
        let b = Matrix::scaling(5.0, 5.0, 5.0);
        let c = Matrix::translation(10.0, 5.0, 7.0);

        // apply rotation first
        let p2 = a * p;
        assert_eq!(p2, Tuple::point(1.0, -1.0, 0.0));

        // then apply scaling
        let p3 = b * p2;
        assert_eq!(p3, Tuple::point(5.0, -5.0, 0.0));

        // then apply translation
        let p4 = c * p3;
        assert_eq!(p4, Tuple::point(15.0, 0.0, 7.0));
    }

    // Chained transformations must be applied in reverse order
    #[test]
    fn test_sequence_chain() {
        let p = Tuple::point(1.0, 0.0, 1.0);
        let a = Matrix::rotation_x(f64::consts::PI / 2.0);
        let b = Matrix::scaling(5.0, 5.0, 5.0);
        let c = Matrix::translation(10.0, 5.0, 7.0);
        let t = c * b * a;
        assert_eq!(t * p, Tuple::point(15.0, 0.0, 7.0));
    }

    // The transformation matrix for the default orientation
    #[test]
    fn test_view_transform() {
        assert_eq!(
            Matrix::view_transform(
                Tuple::point(0.0, 0.0, 0.0),
                Tuple::point(0.0, 0.0, -1.0),
                Tuple::vector(0.0, 1.0, 0.0),
            ),
            Matrix::identity(4)
        );
    }

    // The transformation matrix looking in positive Z direction
    #[test]
    fn test_view_transform_z() {
        assert_eq!(
            Matrix::view_transform(
                Tuple::point(0.0, 0.0, 0.0),
                Tuple::point(0.0, 0.0, 1.0),
                Tuple::vector(0.0, 1.0, 0.0),
            ),
            Matrix::scaling(-1.0, 1.0, -1.0)
        );
    }

    // The transformation matrix moves the world
    #[test]
    fn test_view_transform_move() {
        assert_eq!(
            Matrix::view_transform(
                Tuple::point(0.0, 0.0, 8.0),
                Tuple::point(0.0, 0.0, 0.0),
                Tuple::vector(0.0, 1.0, 0.0),
            ),
            Matrix::translation(0.0, 0.0, -8.0)
        );
    }

    // An arbritary view transformation
    #[test]
    fn test_view_transform_arbritary() {
        assert_eq!(
            Matrix::view_transform(
                Tuple::point(1.0, 3.0, 2.0),
                Tuple::point(4.0, -2.0, 8.0),
                Tuple::vector(1.0, 1.0, 0.0),
            ),
            Matrix::new(vec![
                vec![-0.50709, 0.50709, 0.67612, -2.36643],
                vec![0.76772, 0.60609, 0.12122, -2.82843],
                vec![-0.35857, 0.59761, -0.71714, 0.00000],
                vec![0.00000, 0.00000, 0.00000, 1.00000],
            ])
        );
    }
}
