use crate::parser::Transformations;
use crate::tuple::Tuple;
use crate::utils::almost_eq;
use serde;
use serde::Deserialize;
use std::fmt;
use std::ops::Index;
use std::ops::Mul;

// We only go up to 4x4
const SIZE: usize = 16;

#[derive(Debug, Copy, Clone, Deserialize)]
#[serde(default = "Matrix::default")]
#[serde(from = "Transformations")]
pub struct Matrix {
    columns: usize,
    rows: usize,
    data: [f64; SIZE],
}

impl Matrix {
    // Create a new matrix supporting any dynamic size
    pub fn new(data: Vec<Vec<f64>>) -> Self {
        let rows = data.len();
        assert!(rows > 0);

        // Check all columns are the same length
        // dedup will remove all duplicates, so we should only have one length left in the list
        let mut columns: Vec<usize> = data.iter().map(|l| l.len()).collect();
        columns.dedup();
        assert!(columns.len() == 1);
        assert!(columns[0] > 0);
        assert!(columns.len() * rows <= SIZE);

        // Build a linearized list of all values
        let mut linearized = [0.0 as f64; SIZE];
        for (i, value) in data.iter().flat_map(|row| row.iter()).enumerate() {
            linearized[i] = *value;
        }

        Self {
            columns: columns[0],
            rows: rows,
            data: linearized,
        }
    }

    pub fn size(&self) -> (usize, usize) {
        (self.rows, self.columns)
    }

    pub fn set(&mut self, row: usize, column: usize, value: f64) {
        assert!(row < self.rows);
        assert!(column < self.columns);
        self.data[row * self.columns + column] = value;
    }

    pub fn default() -> Self {
        Matrix::identity(4)
    }

    // Build an identity matrix of any requested size
    pub fn identity(size: usize) -> Self {
        assert!(size <= 4);
        let mut data = [0.0 as f64; SIZE];
        for i in 0..size {
            data[i * size + i] = 1.0;
        }

        Self {
            columns: size,
            rows: size,
            data: data,
        }
    }

    pub fn transpose(&self) -> Self {
        let mut data = [0.0 as f64; SIZE];
        for i in 0..self.rows {
            for j in 0..self.columns {
                data[j * self.rows + i] = self.data[i * self.rows + j];
            }
        }

        Self {
            columns: self.rows,
            rows: self.columns,
            data: data,
        }
    }

    fn determinant(&self) -> f64 {
        // Support 2x2 matrices by calculating directly
        if self.columns == 2 && self.rows == 2 {
            return self.data[0] * self.data[3] - self.data[1] * self.data[2];
        }

        // Calc recursively for higher matrices
        let mut result: f64 = 0.0;
        for i in 0..self.columns {
            result += self.data[i] * self.cofactor(0, i);
        }
        return result;
    }

    fn submatrix(&self, row: usize, column: usize) -> Self {
        assert!(column < self.columns);
        assert!(row < self.rows);

        let mut result = [0.0 as f64; SIZE];
        let mut cpt = 0;
        for i in 0..self.rows {
            for j in 0..self.columns {
                if i != row && j != column {
                    result[cpt] = self.data[i * self.columns + j];
                    cpt += 1;
                }
            }
        }

        Self {
            columns: self.columns - 1,
            rows: self.rows - 1,
            data: result,
        }
    }

    fn minor(&self, row: usize, column: usize) -> f64 {
        // It's simply the determinant of a smaller matrix
        self.submatrix(row, column).determinant()
    }

    fn cofactor(&self, row: usize, column: usize) -> f64 {
        let factor = if (row + column) % 2 == 1 { -1.0 } else { 1.0 };
        factor * self.minor(row, column)
    }

    fn is_invertible(&self) -> bool {
        self.determinant() != 0.0
    }

    pub fn inverse(&self) -> Self {
        // Using a Result might be better here
        assert!(self.is_invertible());

        let mut result = [0.0 as f64; SIZE];
        let d = self.determinant();

        for i in 0..self.rows {
            for j in 0..self.columns {
                // Inverted col/row to transpose directly
                result[j * self.rows + i] = self.cofactor(i, j) / d;
            }
        }

        Self {
            columns: self.columns,
            rows: self.rows,
            data: result,
        }
    }
}

impl Index<usize> for Matrix {
    type Output = [f64];

    fn index(&self, index: usize) -> &Self::Output {
        assert!(index < self.rows);
        let offset = index * self.columns;
        &self.data[offset..offset + self.columns]
    }
}

impl PartialEq for Matrix {
    fn eq(&self, other: &Self) -> bool {
        if self.columns != other.columns || self.rows != other.rows {
            return false;
        }

        // Compare matrices values with almost equality
        self.data
            .iter()
            .zip(other.data.iter())
            .fold(true, |sum, (&x, &y)| sum & almost_eq(x, y))
    }
}

impl Mul<Matrix> for Matrix {
    type Output = Self;

    fn mul(self, other: Matrix) -> Self {
        assert_eq!(self.columns, other.rows);

        let mut result = [0.0 as f64; SIZE];
        for i in 0..self.rows {
            for j in 0..other.columns {
                let mut value: f64 = 0.0;
                for k in 0..self.columns {
                    value += self[i][k] * other[k][j]
                }
                result[i * self.columns + j] = value;
            }
        }

        Self {
            rows: self.rows,
            columns: other.columns,
            data: result,
        }
    }
}

impl Mul<Tuple> for Matrix {
    type Output = Tuple;

    fn mul(self, other: Tuple) -> Tuple {
        assert_eq!(self.rows, 4);
        assert_eq!(self.columns, 4);

        let mut result: [f64; 4] = [0.0; 4];
        for i in 0..4 {
            result[i] = self[i][0] * other.x
                + self[i][1] * other.y
                + self[i][2] * other.z
                + self[i][3] * other.w;
        }

        Tuple::from_array(result)
    }
}

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut out: String = format!("Matrix {}x{}:\n", self.rows, self.columns);

        for i in 0..self.rows {
            for j in 0..self.columns {
                let value = self.data[i * self.columns + j];
                if almost_eq(value, 0.0) {
                    // Weird formatting case where -0.000001
                    // would give -0.00 instead of 0.00
                    out.push_str("0.00")
                } else {
                    out.push_str(format!("{:.2}", value).as_str());
                };
                if j < self.columns - 1 {
                    out.push(' ');
                }
            }
            out.push('\n');
        }

        write!(f, "{}", out)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Constructing and inspecting a 4x4 matrix
    #[test]
    fn test_matrix_4x4() {
        let m: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![5.5, 6.5, 7.5, 8.5],
            vec![9.0, 10.0, 11.0, 12.0],
            vec![13.5, 14.5, 15.5, 16.5],
        ]);
        assert_eq!(m[0][0], 1.0);
        assert_eq!(m[0][3], 4.0);
        assert_eq!(m[1][0], 5.5);
        assert_eq!(m[1][2], 7.5);
        assert_eq!(m[2][2], 11.0);
        assert_eq!(m[3][0], 13.5);
        assert_eq!(m[3][2], 15.5);
    }

    // A 2x2 matrix ought to be representable
    #[test]
    fn test_matrix_2x2() {
        let m: Matrix = Matrix::new(vec![vec![-3.0, 5.0], vec![1.0, -2.0]]);
        assert_eq!(m[0][0], -3.0);
        assert_eq!(m[0][1], 5.0);
        assert_eq!(m[1][0], 1.0);
        assert_eq!(m[1][1], -2.0);
    }

    // A 3x3 matrix ought to be representable
    #[test]
    fn test_matrix_3x3() {
        let m: Matrix = Matrix::new(vec![
            vec![-3.0, 5.0, 0.0],
            vec![1.0, -2.0, -7.0],
            vec![0.0, 1.0, 1.0],
        ]);
        assert_eq!(m[0][0], -3.0);
        assert_eq!(m[1][1], -2.0);
        assert_eq!(m[2][2], 1.0);
    }

    // Matrix equality with identical matrices
    #[test]
    fn test_matrix_equal() {
        let a: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![5.0, 6.0, 7.0, 8.0],
            vec![9.0, 8.0, 7.0, 6.0],
            vec![5.0, 4.0, 3.0, 2.0],
        ]);
        let b: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![5.0, 6.0, 7.0, 8.0],
            vec![9.0, 8.0, 7.0, 6.0],
            vec![5.0, 4.0, 3.0, 2.0],
        ]);
        assert!(a == b);
        assert_eq!(a, b);
    }

    // Matrix equality with different matrices
    #[test]
    fn test_matrix_not_equal() {
        let a: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![5.0, 6.0, 7.0, 8.0],
            vec![9.0, 8.0, 7.0, 6.0],
            vec![5.0, 4.0, 3.0, 2.0],
        ]);
        let b: Matrix = Matrix::new(vec![
            vec![2.0, 3.0, 4.0, 5.0],
            vec![6.0, 7.0, 8.0, 9.0],
            vec![8.0, 7.0, 6.0, 5.0],
            vec![4.0, 3.0, 2.0, 1.0],
        ]);
        assert!(a != b);
    }

    // Multiplying two matrices
    #[test]
    fn test_matrix_mul() {
        let a: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![5.0, 6.0, 7.0, 8.0],
            vec![9.0, 8.0, 7.0, 6.0],
            vec![5.0, 4.0, 3.0, 2.0],
        ]);
        let b: Matrix = Matrix::new(vec![
            vec![-2.0, 1.0, 2.0, 3.0],
            vec![3.0, 2.0, 1.0, -1.0],
            vec![4.0, 3.0, 6.0, 5.0],
            vec![1.0, 2.0, 7.0, 8.0],
        ]);
        assert_eq!(
            a * b,
            Matrix::new(vec![
                vec![20.0, 22.0, 50.0, 48.0],
                vec![44.0, 54.0, 114.0, 108.0],
                vec![40.0, 58.0, 110.0, 102.0],
                vec![16.0, 26.0, 46.0, 42.0],
            ])
        );
    }

    // A matrix multiplied by a tuple
    #[test]
    fn test_matrix_mul_tuple() {
        let a: Matrix = Matrix::new(vec![
            vec![1.0, 2.0, 3.0, 4.0],
            vec![2.0, 4.0, 4.0, 2.0],
            vec![8.0, 6.0, 4.0, 1.0],
            vec![0.0, 0.0, 0.0, 1.0],
        ]);
        let b: Tuple = Tuple::new(1.0, 2.0, 3.0, 1.0);
        assert_eq!(a * b, Tuple::new(18.0, 24.0, 33.0, 1.0));
    }

    // Multiplying a matrix by the identity matrix
    #[test]
    fn test_matrix_mul_indentity() {
        let a: Matrix = Matrix::new(vec![
            vec![0.0, 1.0, 2.0, 4.0],
            vec![1.0, 2.0, 4.0, 8.0],
            vec![2.0, 4.0, 8.0, 16.0],
            vec![4.0, 8.0, 16.0, 32.0],
        ]);
        let b = a.clone() * Matrix::identity(4);
        assert_eq!(a, b);
    }

    // Multiplying the identity matrix by a tuple
    #[test]
    fn test_matrix_mul_identity_tuple() {
        let a: Matrix = Matrix::identity(4);
        let b = Tuple::new(1.0, 2.0, 3.0, 4.0);
        assert_eq!(a * b, b);
    }

    // Transposing a matrix
    #[test]
    fn test_transpose() {
        let a: Matrix = Matrix::new(vec![
            vec![0.0, 9.0, 3.0, 0.0],
            vec![9.0, 8.0, 0.0, 8.0],
            vec![1.0, 8.0, 5.0, 3.0],
            vec![0.0, 0.0, 5.0, 8.0],
        ]);
        assert_eq!(
            a.transpose(),
            Matrix::new(vec![
                vec![0.0, 9.0, 1.0, 0.0],
                vec![9.0, 8.0, 8.0, 0.0],
                vec![3.0, 0.0, 5.0, 5.0],
                vec![0.0, 8.0, 3.0, 8.0],
            ])
        );
    }

    // Transposing the identity matrix
    #[test]
    fn test_transpose_identity() {
        let a = Matrix::identity(4);
        assert_eq!(a, a.transpose());
    }

    // Calculating the determinant of a 2x2 matrix
    #[test]
    fn test_determinant() {
        let a = Matrix::new(vec![vec![1.0, 5.0], vec![-3.0, 2.0]]);
        assert_eq!(a.determinant(), 17.0);
    }

    // A submatrix of a 3x3 matrix is a 2x2 matrix
    #[test]
    fn test_submatrix_3x3() {
        let a = Matrix::new(vec![
            vec![1.0, 5.0, 0.0],
            vec![-3.0, 2.0, 7.0],
            vec![0.0, 6.0, -3.0],
        ]);
        assert_eq!(
            a.submatrix(0, 2),
            Matrix::new(vec![vec![-3.0, 2.0], vec![0.0, 6.0],])
        );
    }

    // A submatrix of a 4x4 matrix is a 3x3 matrix
    #[test]
    fn test_submatrix_4x4() {
        let a = Matrix::new(vec![
            vec![-6.0, 1.0, 1.0, 6.0],
            vec![-8.0, 5.0, 8.0, 6.0],
            vec![-1.0, 0.0, 8.0, 2.0],
            vec![-7.0, 1.0, -1.0, 1.0],
        ]);
        assert_eq!(
            a.submatrix(2, 1),
            Matrix::new(vec![
                vec![-6.0, 1.0, 6.0],
                vec![-8.0, 8.0, 6.0],
                vec![-7.0, -1.0, 1.0],
            ])
        );
    }

    // Calculating a minor of a 3x3 matrix
    #[test]
    fn test_minor_3x3() {
        let a = Matrix::new(vec![
            vec![3.0, 5.0, 0.0],
            vec![2.0, -1.0, -7.0],
            vec![6.0, -1.0, 5.0],
        ]);
        let m = a.minor(1, 0);
        let b = a.submatrix(1, 0);
        assert_eq!(b.determinant(), 25.0);
        assert_eq!(m, 25.0);
    }

    // Calculating a cofactor of a 3x3 matrix
    #[test]
    fn test_cofactor_3x3() {
        let a = Matrix::new(vec![
            vec![3.0, 5.0, 0.0],
            vec![2.0, -1.0, -7.0],
            vec![6.0, -1.0, 5.0],
        ]);
        assert_eq!(a.minor(0, 0), -12.0);
        assert_eq!(a.cofactor(0, 0), -12.0);
        assert_eq!(a.minor(1, 0), 25.0);
        assert_eq!(a.cofactor(1, 0), -25.0);
    }

    // Calculating the determinant of a 3x3 matrix
    #[test]
    fn test_determinant_3x3() {
        let a = Matrix::new(vec![
            vec![1.0, 2.0, 6.0],
            vec![-5.0, 8.0, -4.0],
            vec![2.0, 6.0, 4.0],
        ]);
        assert_eq!(a.cofactor(0, 0), 56.0);
        assert_eq!(a.cofactor(0, 1), 12.0);
        assert_eq!(a.cofactor(0, 2), -46.0);
        assert_eq!(a.determinant(), -196.0);
    }

    // Calculating the determinant of a 4x4 matrix
    #[test]
    fn test_determinant_4x4() {
        let a = Matrix::new(vec![
            vec![-2.0, -8.0, 3.0, 5.0],
            vec![-3.0, 1.0, 7.0, 3.0],
            vec![1.0, 2.0, -9.0, 6.0],
            vec![-6.0, 7.0, 7.0, -9.0],
        ]);
        assert_eq!(a.cofactor(0, 0), 690.0);
        assert_eq!(a.cofactor(0, 1), 447.0);
        assert_eq!(a.cofactor(0, 2), 210.0);
        assert_eq!(a.cofactor(0, 3), 51.0);
        assert_eq!(a.determinant(), -4071.0);
    }

    // Testing an invertible matrix for invertibility
    #[test]
    fn test_invertible() {
        let a = Matrix::new(vec![
            vec![6.0, 4.0, 4.0, 4.0],
            vec![5.0, 5.0, 7.0, 6.0],
            vec![4.0, -9.0, 3.0, -7.0],
            vec![9.0, 1.0, 7.0, -6.0],
        ]);
        assert_eq!(a.determinant(), -2120.0);
        assert!(a.is_invertible());
    }

    // Testing a noninvertible matrix for invertibility
    #[test]
    fn test_non_invertible() {
        let a = Matrix::new(vec![
            vec![-4.0, 2.0, -2.0, -3.0],
            vec![9.0, 6.0, 2.0, 6.0],
            vec![0.0, -5.0, 1.0, -5.0],
            vec![0.0, 0.0, 0.0, 0.0],
        ]);
        assert_eq!(a.determinant(), 0.0);
        assert!(!a.is_invertible());
    }

    //  Calculating the inverse of a matrix
    #[test]
    fn test_inverse_1() {
        let a = Matrix::new(vec![
            vec![-5.0, 2.0, 6.0, -8.0],
            vec![1.0, -5.0, 1.0, 8.0],
            vec![7.0, 7.0, -6.0, -7.0],
            vec![1.0, -3.0, 7.0, 4.0],
        ]);
        let b = a.inverse();
        assert_eq!(a.determinant(), 532.0);
        assert_eq!(a.cofactor(3, 2), 105.0);
        assert_eq!(a.cofactor(2, 3), -160.0);
        assert!(almost_eq(b[3][2], -160.0 / 532.0));
        assert!(almost_eq(b[2][3], 105.0 / 532.0));
        assert_eq!(
            b,
            Matrix::new(vec![
                vec![0.21805, 0.45113, 0.24060, -0.04511],
                vec![-0.80827, -1.45677, -0.44361, 0.52068],
                vec![-0.07895, -0.22368, -0.05263, 0.19737],
                vec![-0.52256, -0.81391, -0.30075, 0.30639],
            ])
        );
    }

    // Calculating the inverse of another matrix
    #[test]
    fn test_inverse_2() {
        let a = Matrix::new(vec![
            vec![8.0, -5.0, 9.0, 2.0],
            vec![7.0, 5.0, 6.0, 1.0],
            vec![-6.0, 0.0, 9.0, 6.0],
            vec![-3.0, 0.0, -9.0, -4.0],
        ]);
        assert_eq!(
            a.inverse(),
            Matrix::new(vec![
                vec![-0.15385, -0.15385, -0.28205, -0.53846],
                vec![-0.07692, 0.12308, 0.02564, 0.03077],
                vec![0.35897, 0.35897, 0.43590, 0.92308],
                vec![-0.69231, -0.69231, -0.76923, -1.92308],
            ])
        );
    }

    // Calculating the inverse of a third matrix
    #[test]
    fn test_inverse_3() {
        let a = Matrix::new(vec![
            vec![9.0, 3.0, 0.0, 9.0],
            vec![-5.0, -2.0, -6.0, -3.0],
            vec![-4.0, 9.0, 6.0, 4.0],
            vec![-7.0, 6.0, 6.0, 2.0],
        ]);
        assert_eq!(
            a.inverse(),
            Matrix::new(vec![
                vec![-0.04074, -0.07778, 0.14444, -0.22222],
                vec![-0.07778, 0.03333, 0.36667, -0.33333],
                vec![-0.02901, -0.14630, -0.10926, 0.12963],
                vec![0.17778, 0.06667, -0.26667, 0.33333],
            ])
        );
    }

    // Multiplying a product by its inverse
    #[test]
    fn test_inverse_product() {
        let a = Matrix::new(vec![
            vec![3.0, -9.0, 7.0, 3.0],
            vec![3.0, -8.0, 2.0, -9.0],
            vec![-4.0, 4.0, 4.0, 1.0],
            vec![-6.0, 5.0, -1.0, 1.0],
        ]);
        let b = Matrix::new(vec![
            vec![8.0, 2.0, 2.0, 2.0],
            vec![3.0, -1.0, 7.0, 0.0],
            vec![7.0, 0.0, 5.0, 4.0],
            vec![6.0, -2.0, 0.0, 5.0],
        ]);
        let c = a.clone() * b.clone();
        let x = c * b.clone().inverse();
        assert_eq!(x, a.clone());
    }
}
