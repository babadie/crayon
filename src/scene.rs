use crate::canvas::Canvas;
use crate::color::Color;
use crate::light::PointLight;
use crate::matrix::Matrix;
use crate::ray::{hit, sort_intersections, Hittable, IntersectionState, Intersections, Ray};
use crate::shapes::sphere::Sphere;
use crate::shapes::Object;
use crate::tuple::Tuple;
use rayon::prelude::*;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct World {
    objects: Vec<Object>,
    lights: Vec<PointLight>,
}

impl World {
    // Create a new empty world
    pub fn new() -> Self {
        World {
            objects: Vec::new(),
            lights: Vec::new(),
        }
    }

    pub fn add_light(&mut self, light: PointLight) {
        self.lights.push(light);
    }

    pub fn add_object(&mut self, obj: Object) {
        self.objects.push(obj);
    }

    // Find all the intersections for that ray in the current world
    fn intersect(&self, ray: Ray) -> Intersections {
        let mut intersections = Vec::new();

        for obj in &self.objects {
            let xs = match obj {
                Object::Sphere(s) => s.intersect(ray),
                Object::Plane(p) => p.intersect(ray),
            };
            intersections.extend(xs);
        }

        // Intersections must be sorted for easier reflection & refraction (and unit tests !)
        return sort_intersections(intersections);
    }

    // Helper to calculate the color from a given pre computed intersection state
    fn shade_hit(&self, state: IntersectionState) -> Color {
        let mut color = Color::black();
        let shape = match state.object {
            Object::Sphere(s) => s.shape,
            Object::Plane(p) => p.shape,
        };

        for light in &self.lights {
            color = color
                + light.apply(
                    shape.material,
                    state.point,
                    state.eyev,
                    state.normalv,
                    self.is_shadowed(state.over_point, *light),
                );
        }
        return color;
    }

    // Workflow to get the color for a given ray in current world
    fn color_at(&self, ray: Ray) -> Color {
        // Find the hit for that ray in this world
        let intersections = self.intersect(ray);

        return match hit(intersections) {
            // Use black by default when the ray misses
            None => Color::black(),
            Some(intersection) => {
                // Calc the color for that given ray at the first intersection
                let state = intersection.precompute(ray);
                return self.shade_hit(state);
            }
        };
    }

    // Determine if a given point in the world is in shadow
    // of any objects in the world, for a specific light
    fn is_shadowed(&self, point: Tuple, light: PointLight) -> bool {
        assert!(point.is_point());

        // Determine shadow vector going from the point towards the light
        let shadow_v = light.position - point;
        let distance = shadow_v.magnitude();
        let direction = shadow_v.normalize();

        // Create a ray shooting towards the light source
        let shadow_ray = Ray::new(point, direction);
        let intersections = self.intersect(shadow_ray);

        // If there is a hit before reaching the light source
        // the point is shadowed
        match hit(intersections) {
            Some(h) if h.time < distance => true,
            _ => false,
        }
    }
}

impl Default for World {
    // Create a world with a default light and 2 spheres
    fn default() -> Self {
        let mut world = Self::new();

        // Default light
        world.lights.push(PointLight::new(
            Color::white(),
            Tuple::point(-10.0, 10.0, -10.0),
        ));

        // Default sphere n°1
        let mut s1 = Sphere::simple();
        s1.shape.material.color = Color::new(0.8, 1.0, 0.6);
        s1.shape.material.diffuse = 0.7;
        s1.shape.material.specular = 0.2;
        world.add_object(Object::from(s1));

        // Default sphere n°2
        let mut s2 = Sphere::simple();
        s2.shape.transform = Matrix::scaling(0.5, 0.5, 0.5);
        world.add_object(Object::from(s2));

        return world;
    }
}

impl PartialEq for World {
    fn eq(&self, other: &Self) -> bool {
        return self.lights == other.lights && self.objects == other.objects;
    }
}

#[derive(Deserialize, Debug)]
pub struct Camera {
    hsize: usize,
    vsize: usize,
    field_of_view: f64,

    pub transform: Matrix,
}

impl Camera {
    pub fn new(hsize: usize, vsize: usize, field_of_view: f64) -> Self {
        Self {
            hsize: hsize,
            vsize: vsize,
            field_of_view: field_of_view,
            transform: Matrix::identity(4),
        }
    }

    pub fn set_size(&mut self, hsize: usize, vsize: usize) {
        self.hsize = hsize;
        self.vsize = vsize;
    }

    // TODO: this should be cached (computed once)
    fn compute_pixel_size(&self) -> (f64, f64, f64) {
        // Calc the pixel size when the canvas is one unit away
        let half_view = (self.field_of_view / 2.0).tan();

        let aspect_ratio: f64 = (self.hsize as f64) / (self.vsize as f64);

        let (half_width, half_height) = if aspect_ratio >= 1.0 {
            // Horizontal canvas
            (half_view, half_view / aspect_ratio)
        } else {
            // Vertical canvas
            (half_view * aspect_ratio, half_view)
        };

        // We assume pixels are square, so width=height here.
        let pixel_size = (half_width * 2.0) / (self.hsize as f64);

        return (half_width, half_height, pixel_size);
    }

    // Build a ray for a specific pixel in the canvas
    fn ray_for_pixel(&self, x: usize, y: usize) -> Ray {
        assert!(x < self.hsize);
        assert!(y < self.vsize);

        let (half_width, half_height, pixel_size) = self.compute_pixel_size();

        // Calculate the precise center of the pixel
        let offset_x = ((x as f64) + 0.5) * pixel_size;
        let offset_y = ((y as f64) + 0.5) * pixel_size;

        // Convert pixel coordinates in world space
        let world_x = half_width - offset_x;
        let world_y = half_height - offset_y;

        // Then transform the canvas point & origin
        // to compute the ray's direction vector
        let inv = self.transform.inverse();
        let pixel = inv.clone() * Tuple::point(world_x, world_y, -1.0);
        let origin = inv * Tuple::point(0.0, 0.0, 0.0);
        let direction = (pixel - origin).normalize();

        return Ray::new(origin, direction);
    }

    // Render a given world to a new canvas, using data parallelism with rayon
    pub fn render(&self, world: World) -> Canvas {
        // Put all (x, y) coords in an iterator
        // to make it usable with rayon
        let points: Vec<(usize, usize)> = (0..self.hsize)
            .map(|x| (0..self.vsize).map(move |y| (x, y)))
            .flatten()
            .collect();

        // Calculate the color for each ray shot by the camera
        let colors: Vec<(usize, usize, Color)> = points
            .par_iter() // This is where rayon starts data parallelism and burns cores
            .map(|(x, y)| {
                let ray = self.ray_for_pixel(*x, *y);
                (*x, *y, world.color_at(ray))
            })
            .collect();

        // Finally we apply all the calculated colors to the canvas
        let mut canvas = Canvas::new(self.hsize, self.vsize);
        for (x, y, color) in colors {
            canvas.set_pixel(x, y, color);
        }
        return canvas;
    }
}

impl PartialEq for Camera {
    fn eq(&self, other: &Self) -> bool {
        return self.hsize == other.hsize
            && self.vsize == other.vsize
            && self.field_of_view == other.field_of_view
            && self.transform == other.transform;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ray::Intersection;
    use crate::utils::almost_eq;

    // Creating an empty world
    #[test]
    fn test_empty_world() {
        let w = World::new();
        assert_eq!(w.objects.len(), 0);
        assert_eq!(w.lights.len(), 0);
    }

    // Creating the default world
    #[test]
    fn test_default_world() {
        let w = World::default();
        assert_eq!(w.objects.len(), 2);
        assert_eq!(w.lights.len(), 1);
        assert_eq!(
            w.lights[0],
            PointLight::new(Color::white(), Tuple::point(-10.0, 10.0, -10.0))
        );
        if let Object::Sphere(s1) = w.objects[0] {
            assert_eq!(s1.shape.material.color, Color::new(0.8, 1.0, 0.6));
            assert_eq!(s1.shape.material.diffuse, 0.7);
            assert_eq!(s1.shape.material.specular, 0.2);
            assert_eq!(s1.shape.transform, Matrix::identity(4));
        } else {
            panic!("Missing sphere 1");
        }
        if let Object::Sphere(s2) = w.objects[1] {
            assert_eq!(s2.shape.material.color, Color::white());
            assert_eq!(s2.shape.material.diffuse, 0.9);
            assert_eq!(s2.shape.material.specular, 0.9);
            assert_eq!(s2.shape.transform, Matrix::scaling(0.5, 0.5, 0.5));
        } else {
            panic!("Missing sphere 2");
        }
    }

    // Intersect a world with a ray
    #[test]
    fn test_intersects() {
        let w = World::default();
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let xs = w.intersect(r);
        assert_eq!(xs.len(), 4);
        assert_eq!(xs[0].time, 4.0);
        assert_eq!(xs[1].time, 4.5);
        assert_eq!(xs[2].time, 5.5);
        assert_eq!(xs[3].time, 6.0);
    }

    // Shading an intersection
    #[test]
    fn test_shading_intersection() {
        let w = World::default();
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let i = Intersection::new(4.0, w.objects[0]);
        let state = i.precompute(r);
        assert_eq!(w.shade_hit(state), Color::new(0.38066, 0.47583, 0.2855));
    }

    // Shading an intersection from the inside
    #[test]
    fn test_shading_intersection_inside() {
        let mut w = World::default();
        w.lights[0] = PointLight::new(Color::white(), Tuple::point(0.0, 0.25, 0.0));
        let r = Ray::new(Tuple::point(0.0, 0.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        let i = Intersection::new(0.5, w.objects[1]);
        let state = i.precompute(r);
        assert_eq!(w.shade_hit(state), Color::new(0.90498, 0.90498, 0.90498));
    }

    // The color when a ray misses
    #[test]
    fn test_color_at_miss() {
        let w = World::default();
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 1.0, 0.0));
        assert_eq!(w.color_at(r), Color::black());
    }

    // The color when a ray hits
    #[test]
    fn test_color_at_hits() {
        let w = World::default();
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        assert_eq!(w.color_at(r), Color::new(0.38066, 0.47583, 0.2855));
    }

    // The color with an intersection behind the ray
    // The ray hits the second object, and won't go further
    #[test]
    fn test_color_at_intersection() {
        let mut w = World::default();
        if let Object::Sphere(mut s1) = w.objects[0] {
            s1.shape.material.ambient = 1.0;
            w.objects[0] = Object::from(s1);
        } else {
            panic!("Missing sphere 1");
        }
        if let Object::Sphere(mut s2) = w.objects[1] {
            s2.shape.material.ambient = 1.0;
            w.objects[1] = Object::from(s2);
        } else {
            panic!("Missing sphere 2");
        }
        let r = Ray::new(Tuple::point(0.0, 0.0, 0.75), Tuple::vector(0.0, 0.0, -1.0));
        assert_eq!(w.color_at(r), Color::white());
    }

    // Constructing a camera
    #[test]
    fn test_camera_default() {
        let c = Camera::new(160, 120, std::f64::consts::PI / 2.0);
        assert_eq!(c.hsize, 160);
        assert_eq!(c.vsize, 120);
        assert_eq!(c.field_of_view, std::f64::consts::PI / 2.0);
        assert_eq!(c.transform, Matrix::identity(4));
    }

    // The pixel size for a horizontal canvas
    #[test]
    fn test_camera_horizontal() {
        let c = Camera::new(200, 125, std::f64::consts::PI / 2.0);
        let (_, _, pixel_size) = c.compute_pixel_size();
        assert!(almost_eq(pixel_size, 0.01));
    }

    // The pixel size for a vertical canvas
    #[test]
    fn test_camera_vertical() {
        let c = Camera::new(125, 200, std::f64::consts::PI / 2.0);
        let (_, _, pixel_size) = c.compute_pixel_size();
        assert!(almost_eq(pixel_size, 0.01));
    }

    // Constructing a ray through the center of the canvas
    #[test]
    fn test_camera_ray_center() {
        let c = Camera::new(201, 101, std::f64::consts::PI / 2.0);
        let r = c.ray_for_pixel(100, 50);
        assert_eq!(r.origin, Tuple::point(0.0, 0.0, 0.0));
        assert_eq!(r.direction, Tuple::vector(0.0, 0.0, -1.0));
    }

    // Constructing a ray through a corner of the canvas
    #[test]
    fn test_camera_ray_corner() {
        let c = Camera::new(201, 101, std::f64::consts::PI / 2.0);
        let r = c.ray_for_pixel(0, 0);
        assert_eq!(r.origin, Tuple::point(0.0, 0.0, 0.0));
        assert_eq!(r.direction, Tuple::vector(0.66519, 0.33259, -0.66851));
    }

    // Constructing a ray when the camera is transformed
    #[test]
    fn test_camera_ray_transform() {
        let mut c = Camera::new(201, 101, std::f64::consts::PI / 2.0);
        c.transform =
            Matrix::rotation_y(std::f64::consts::PI / 4.0) * Matrix::translation(0.0, -2.0, 5.0);
        let r = c.ray_for_pixel(100, 50);
        assert_eq!(r.origin, Tuple::point(0.0, 2.0, -5.0));
        assert_eq!(
            r.direction,
            Tuple::vector((2.0 as f64).sqrt() / 2.0, 0.0, -(2.0 as f64).sqrt() / 2.0)
        );
    }

    // Rendering a world with a camera
    #[test]
    fn test_render_world() {
        let w = World::default();
        let mut c = Camera::new(11, 11, std::f64::consts::PI / 2.0);
        c.transform = Matrix::view_transform(
            Tuple::point(0.0, 0.0, -5.0),
            Tuple::point(0.0, 0.0, 0.0),
            Tuple::vector(0.0, 1.0, 0.0),
        );
        let image = c.render(w);
        assert_eq!(image.get_pixel(5, 5), Color::new(0.38066, 0.47583, 0.2855));
    }

    // There is no shadow when nothing is collinear with point and light
    #[test]
    fn test_shadow_1() {
        let w = World::default();
        let p = Tuple::point(0.0, 10.0, 0.0);
        assert!(!w.is_shadowed(p, w.lights[0]));
    }

    // There is shadow when an object is between the point and light
    #[test]
    fn test_shadow_2() {
        let w = World::default();
        let p = Tuple::point(10.0, -10.0, 10.0);
        assert!(w.is_shadowed(p, w.lights[0]));
    }

    // There is no shadow when an object is behind the light
    #[test]
    fn test_shadow_3() {
        let w = World::default();
        let p = Tuple::point(-20.0, 20.0, -20.0);
        assert!(!w.is_shadowed(p, w.lights[0]));
    }

    // There is no shadow when an object is behind the point
    #[test]
    fn test_shadow_4() {
        let w = World::default();
        let p = Tuple::point(-2.0, 2.0, -2.0);
        assert!(!w.is_shadowed(p, w.lights[0]));
    }

    // shade_hit() is given an intersection in shadow
    #[test]
    fn test_shade_hit_in_shadow() {
        let mut w = World::new();
        w.add_light(PointLight::new(
            Color::white(),
            Tuple::point(0.0, 0.0, -10.0),
        ));
        w.add_object(Object::from(Sphere::simple()));
        let mut s = Sphere::simple();
        s.shape.transform = Matrix::translation(0.0, 0.0, 10.0);
        w.add_object(Object::from(s));

        let r = Ray::new(Tuple::point(0.0, 0.0, 5.0), Tuple::vector(0.0, 0.0, 1.0));
        let i = Intersection::new(4.0, Object::from(w.objects[1]));
        let state = i.precompute(r);

        assert_eq!(w.shade_hit(state), Color::new(0.1, 0.1, 0.1));
    }
}
