use crate::color::Color;
use std::fs::File;
use std::io::prelude::*;

pub struct Canvas {
    pub width: usize,
    pub height: usize,
    pixels: Vec<Color>,
}

impl Canvas {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            width: width,
            height: height,
            pixels: vec![Color::new(0.0, 0.0, 0.0); width * height],
        }
    }

    pub fn get_pixel(self, x: usize, y: usize) -> Color {
        assert!(x < self.width);
        assert!(y < self.height);
        self.pixels[x + y * self.width]
    }

    pub fn set_pixel(&mut self, x: usize, y: usize, color: Color) {
        assert!(x < self.width);
        assert!(y < self.height);
        self.pixels[x + y * self.width] = color;
    }

    pub fn build_ppm(self) -> String {
        let mut ppm = vec![
            // Build the PPM header
            String::from("P3"),
            format!("{} {}", self.width, self.height),
            String::from("255"),
            // First line of data populated below
            String::from(""),
        ];

        // Add all the pixels in the ppm payload
        for (i, pixel) in self.pixels.iter().enumerate() {
            // Each pixel has 3 colors to add in the PPM payload
            // * separated with a space
            // * one or more PPM line per raster line
            // * a PPM line is 70 chars max
            for rgb in pixel.to_ppm() {
                let index = ppm.len() - 1;
                let line = &mut ppm[index];

                // Jump to next line when current one has not enough space left
                if line.len() + rgb.len() >= 70 {
                    ppm.push(rgb);
                    continue;
                }

                // Add a new color on the current line
                // Separated with a space when a previous value is present
                if !line.is_empty() {
                    line.push(' ');
                }
                line.push_str(&rgb);
            }

            // Start a new line for each new raster line
            if i % self.width == self.width - 1 {
                ppm.push(String::from(""));
            }
        }

        // Join ppm data with new lines
        ppm.join("\n")
    }

    pub fn save(self, path: &str) -> std::io::Result<()> {
        let mut f = File::create(path)?;
        f.write_all(self.build_ppm().as_bytes())?;
        f.sync_all()?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Creating a canvas
    #[test]
    fn test_init() {
        let c: Canvas = Canvas::new(10, 20);
        assert_eq!(c.width, 10);
        assert_eq!(c.height, 20);
        assert_eq!(c.pixels.len(), 200);

        let black: Color = Color::new(0.0, 0.0, 0.0);
        for pixel in c.pixels.iter() {
            assert_eq!(*pixel, black);
        }
    }

    // Writing pixels to a canvas
    #[test]
    fn test_write() {
        let mut c: Canvas = Canvas::new(10, 20);
        let red = Color::new(1.0, 0.0, 0.0);
        c.set_pixel(2, 3, red);
        assert_eq!(c.get_pixel(2, 3), red);
    }

    // Constructing the PPM header
    #[test]
    fn test_ppm_header() {
        let c: Canvas = Canvas::new(5, 3);
        let ppm: String = c.build_ppm();

        // Check only the first 3 lines
        let lines: Vec<&str> = ppm.lines().take(3).collect();
        assert_eq!(lines, ["P3", "5 3", "255",]);
    }

    // Constructing the PPM pixel data
    #[test]
    fn test_ppm_data() {
        let mut c: Canvas = Canvas::new(5, 3);
        c.set_pixel(0, 0, Color::new(1.5, 0.0, 0.0));
        c.set_pixel(2, 1, Color::new(0.0, 0.5, 0.0));
        c.set_pixel(4, 2, Color::new(-0.5, 0.0, 1.0));
        let ppm: String = c.build_ppm();

        // Check lines after the header
        let lines: Vec<&str> = ppm.lines().skip(3).take(3).collect();
        assert_eq!(
            lines,
            [
                "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
                "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0",
                "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255",
            ]
        );
    }

    // Splitting long lines in PPM files
    #[test]
    fn test_ppm_long_lines() {
        let mut c: Canvas = Canvas::new(10, 2);
        let color = Color::new(1.0, 0.8, 0.6);

        for x in 0..c.width {
            for y in 0..c.height {
                c.set_pixel(x, y, color);
            }
        }

        let ppm: String = c.build_ppm();

        // Check lines after the header
        let lines: Vec<&str> = ppm.lines().skip(3).take(4).collect();
        assert_eq!(
            lines,
            [
                "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
                "153 255 204 153 255 204 153 255 204 153 255 204 153",
                "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
                "153 255 204 153 255 204 153 255 204 153 255 204 153",
            ]
        );
    }

    // PPM files are terminated by a newline character
    #[test]
    fn test_ppm_footer() {
        let c: Canvas = Canvas::new(5, 3);
        let ppm: String = c.build_ppm();
        assert!(ppm.ends_with("\n"));
    }
}
