use crate::matrix::Matrix;
use crate::shapes::Object;
use crate::tuple::Tuple;
use crate::EPSILON;
use std::f64;

#[derive(Copy, Clone, Debug)]
pub struct Ray {
    pub origin: Tuple,
    pub direction: Tuple,
}

impl Ray {
    pub fn new(origin: Tuple, direction: Tuple) -> Self {
        assert!(origin.is_point());
        assert!(direction.is_vector());
        Self {
            origin: origin,
            direction: direction,
        }
    }

    pub fn position(&self, time: f64) -> Tuple {
        self.origin + self.direction * time
    }

    pub fn transform(&self, transformation: Matrix) -> Self {
        let origin = transformation.clone() * self.origin;
        let direction = transformation * self.direction;
        Self {
            origin: origin,
            direction: direction,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Intersection {
    pub time: f64,
    // Only support spheres right now
    pub object: Object,
}

pub struct IntersectionState {
    time: f64,
    pub object: Object,
    pub point: Tuple,
    pub over_point: Tuple, // Point with offset for shadows (to avoid acne)
    pub eyev: Tuple,
    pub normalv: Tuple,
    inside: bool,
}

impl Intersection {
    pub fn new(time: f64, object: Object) -> Self {
        Self {
            time: time,
            object: object,
        }
    }

    // Prepare the calculation of a point in world space where the
    // intersection occured, along with the eye vector (pointing back to
    // the camera) and the normal vector
    pub fn precompute(&self, ray: Ray) -> IntersectionState {
        let point = ray.position(self.time);
        let normalv = match self.object {
            Object::Sphere(s) => s.normal_at(point),
            Object::Plane(p) => p.normal_at(point),
        };
        let eyev = -ray.direction;
        let inside: bool = normalv.dot(eyev) < 0.0;
        let normalv = if inside { -normalv } else { normalv };
        IntersectionState {
            time: self.time,
            object: self.object,
            point: point,
            over_point: point + normalv * EPSILON,
            eyev: eyev,
            // Invert the normal vector when inside the object
            normalv: normalv,
            inside: inside,
        }
    }
}

impl PartialEq for Intersection {
    fn eq(&self, other: &Self) -> bool {
        self.time == other.time && self.object == other.object
    }
}

// A simple list of Intersection allowing to find the first hit point
pub type Intersections = Vec<Intersection>;

// Sort a list of intersection, from closest point to furthest
pub fn sort_intersections(mut points: Intersections) -> Intersections {
    // Cannot use sort_by_key as f64 does not implement Ord
    // because it could have NaN (we should never have it here)
    points.sort_by(|x, y| x.time.partial_cmp(&y.time).unwrap());
    return points;
}

pub fn hit(xs: Intersections) -> Option<Intersection> {
    // Only keep non negative times
    // And sort those remaining intersection by time
    let mut points = vec![];
    for point in xs {
        if point.time >= 0.0 {
            points.push(point);
        }
    }

    // Stop there when no points is available
    if points.len() == 0 {
        return None;
    }

    // Find the smallest one
    let points = sort_intersections(points);
    return Some(points[0].clone());
}

// Trait to be implement by all 3D primitives (shapes)
pub trait Hittable {
    // An intersection is a potential tuple of N time values
    // where the ray intersect the object
    fn intersect(self, ray: Ray) -> Intersections;

    // Calc the normal vector for a given point on the object
    fn normal_at(&self, world_point: Tuple) -> Tuple;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::shapes::sphere::Sphere;

    // Creating and querying a ray
    #[test]
    fn test_create() {
        let origin = Tuple::point(1.0, 2.0, 3.0);
        let direction = Tuple::vector(4.0, 5.0, 6.0);
        let r = Ray::new(origin, direction);
        assert_eq!(r.origin, origin);
        assert_eq!(r.direction, direction);
    }

    // Computing a point from a distance
    #[test]
    fn test_position() {
        let r = Ray::new(Tuple::point(2.0, 3.0, 4.0), Tuple::vector(1.0, 0.0, 0.0));
        assert_eq!(r.position(0.0), Tuple::point(2.0, 3.0, 4.0));
        assert_eq!(r.position(1.0), Tuple::point(3.0, 3.0, 4.0));
        assert_eq!(r.position(-1.0), Tuple::point(1.0, 3.0, 4.0));
        assert_eq!(r.position(2.5), Tuple::point(4.5, 3.0, 4.0));
    }

    // An intersection encapsulates t and object
    #[test]
    fn test_intersection() {
        let s = Object::from(Sphere::simple());
        let i = Intersection::new(3.5, s);
        assert_eq!(i.time, 3.5);
        assert_eq!(i.object, s);
    }

    // Aggregating intersections
    #[test]
    fn test_intersections() {
        let s = Object::from(Sphere::simple());
        let i1 = Intersection::new(1.0, s);
        let i2 = Intersection::new(2.0, s);
        let xs: Intersections = vec![i1, i2];
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, 1.0);
        assert_eq!(xs[1].time, 2.0);
    }

    // A ray intersects a sphere at two points
    #[test]
    fn test_intersect_sphere() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, 4.0);
        assert_eq!(xs[1].time, 6.0);
    }

    // A ray intersects a sphere at a tangent
    #[test]
    fn test_intersect_sphere_tangent() {
        let r = Ray::new(Tuple::point(0.0, 1.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, 5.0);
        assert_eq!(xs[1].time, 5.0);
    }

    // A ray misses a sphere
    #[test]
    fn test_intersect_sphere_miss() {
        let r = Ray::new(Tuple::point(0.0, 2.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 0);
    }

    // A ray originates inside a sphere
    #[test]
    fn test_intersect_sphere_inside() {
        let r = Ray::new(Tuple::point(0.0, 0.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, -1.0);
        assert_eq!(xs[1].time, 1.0);
    }

    // A sphere is behind a ray
    #[test]
    fn test_intersect_sphere_behind() {
        let r = Ray::new(Tuple::point(0.0, 0.0, 5.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, -6.0);
        assert_eq!(xs[1].time, -4.0);
    }

    // Intersect sets the object on the intersection
    #[test]
    fn test_intersect_set_object() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let s = Sphere::simple();
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].object, Object::from(s));
        assert_eq!(xs[1].object, Object::from(s));
    }

    // The hit, when all intersections have positive t
    #[test]
    fn test_hit_positive() {
        let s = Object::from(Sphere::simple());
        let i1 = Intersection::new(1.0, s);
        let i2 = Intersection::new(2.0, s);
        let xs = Intersections::from(vec![i1, i2]);
        let i = hit(xs).expect("No hit");
        assert_eq!(i, i1);
    }

    // The hit, when some intersections have negative t
    #[test]
    fn test_hit_negative_positive() {
        let s = Object::from(Sphere::simple());
        let i1 = Intersection::new(-1.0, s);
        let i2 = Intersection::new(1.0, s);
        let xs = Intersections::from(vec![i1, i2]);
        let i = hit(xs).expect("No hit");
        assert_eq!(i, i2);
    }

    // The hit, when all intersections have negative t
    #[test]
    fn test_hit_negative() {
        let s = Object::from(Sphere::simple());
        let i1 = Intersection::new(-2.0, s);
        let i2 = Intersection::new(-1.0, s);
        let xs = Intersections::from(vec![i1, i2]);
        let i = hit(xs);
        assert_eq!(i, None);
    }

    // The hit is always the lowest nonnegative intersection
    #[test]
    fn test_hit_sort() {
        let s = Object::from(Sphere::simple());
        let i1 = Intersection::new(5.0, s);
        let i2 = Intersection::new(7.0, s);
        let i3 = Intersection::new(-3.0, s);
        let i4 = Intersection::new(2.0, s);
        let xs = Intersections::from(vec![i1, i2, i3, i4]);
        let i = hit(xs).expect("No hit");
        assert_eq!(i, i4);
    }

    // Translating a ray
    #[test]
    fn test_translation() {
        let r = Ray::new(Tuple::point(1.0, 2.0, 3.0), Tuple::vector(0.0, 1.0, 0.0));
        let m = Matrix::translation(3.0, 4.0, 5.0);
        let r2 = r.transform(m);
        assert_eq!(r2.origin, Tuple::point(4.0, 6.0, 8.0));
        assert_eq!(r2.direction, Tuple::vector(0.0, 1.0, 0.0));
    }

    // Scaling a ray
    #[test]
    fn test_scaling() {
        let r = Ray::new(Tuple::point(1.0, 2.0, 3.0), Tuple::vector(0.0, 1.0, 0.0));
        let m = Matrix::scaling(2.0, 3.0, 4.0);
        let r2 = r.transform(m);
        assert_eq!(r2.origin, Tuple::point(2.0, 6.0, 12.0));
        assert_eq!(r2.direction, Tuple::vector(0.0, 3.0, 0.0));
    }

    // Intersecting a scaled sphere with a ray
    #[test]
    fn test_intersect_scaled() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let mut s = Sphere::simple();
        s.shape.transform = Matrix::scaling(2.0, 2.0, 2.0);
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].time, 3.0);
        assert_eq!(xs[1].time, 7.0);
    }

    // Intersecting a translated sphere with a ray
    #[test]
    fn test_intersect_translated() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let mut s = Sphere::simple();
        s.shape.transform = Matrix::translation(5.0, 0.0, 0.0);
        let xs = s.intersect(r);
        assert_eq!(xs.len(), 0);
    }

    // Precompute the state of an intersection
    #[test]
    fn test_precompute() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let obj = Sphere::simple();
        let i = Intersection::new(4.0, Object::from(obj));
        let state = i.precompute(r);
        assert_eq!(state.time, 4.0);
        assert_eq!(state.object, Object::from(obj));
        assert_eq!(state.point, Tuple::point(0.0, 0.0, -1.0));
        assert_eq!(state.eyev, Tuple::vector(0.0, 0.0, -1.0));
        assert_eq!(state.normalv, Tuple::vector(0.0, 0.0, -1.0));
        assert!(!state.inside);
    }

    // Precompute the hit when an intersection occurs on the inside
    #[test]
    fn test_precompute_inside() {
        let r = Ray::new(Tuple::point(0.0, 0.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        let obj = Sphere::simple();
        let i = Intersection::new(1.0, Object::from(obj));
        let state = i.precompute(r);
        assert_eq!(state.time, 1.0);
        assert_eq!(state.object, Object::from(obj));
        assert_eq!(state.point, Tuple::point(0.0, 0.0, 1.0));
        assert_eq!(state.eyev, Tuple::vector(0.0, 0.0, -1.0));
        assert_eq!(state.normalv, Tuple::vector(0.0, 0.0, -1.0));
        assert!(state.inside);
    }

    // The hit should offset the point
    #[test]
    fn test_hit_offset() {
        let r = Ray::new(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        let mut obj = Sphere::simple();
        obj.shape.transform = Matrix::translation(0.0, 0.0, 1.0);
        let i = Intersection::new(5.0, Object::from(obj));
        let state = i.precompute(r);
        assert!(state.over_point.z < -EPSILON / 2.0);
        assert!(state.point.z > state.over_point.z);
    }
}
