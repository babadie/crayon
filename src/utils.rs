pub fn almost_eq(x: f64, y: f64) -> bool {
    (x - y).abs() < 0.0001
}
