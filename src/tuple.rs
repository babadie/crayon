use crate::utils::almost_eq;
use serde::Deserialize;
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Debug, Copy, Clone, Deserialize)]
pub struct Tuple {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

impl PartialEq for Tuple {
    fn eq(&self, other: &Self) -> bool {
        almost_eq(self.x, other.x)
            && almost_eq(self.y, other.y)
            && almost_eq(self.z, other.z)
            && almost_eq(self.w, other.w)
    }
}

impl Add for Tuple {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }
}

impl Sub for Tuple {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
            w: self.w - other.w,
        }
    }
}

impl Neg for Tuple {
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            x: -1.0 * self.x,
            y: -1.0 * self.y,
            z: -1.0 * self.z,
            w: -1.0 * self.w,
        }
    }
}

impl Mul<f64> for Tuple {
    type Output = Self;

    fn mul(self, other: f64) -> Self {
        Self {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
            w: self.w * other,
        }
    }
}

impl Div<f64> for Tuple {
    type Output = Self;

    fn div(self, other: f64) -> Self {
        Self {
            x: self.x / other,
            y: self.y / other,
            z: self.z / other,
            w: self.w / other,
        }
    }
}

impl Tuple {
    pub fn new(x: f64, y: f64, z: f64, w: f64) -> Self {
        Self {
            x: x,
            y: y,
            z: z,
            w: w,
        }
    }

    pub fn from_array(data: [f64; 4]) -> Self {
        Self {
            x: data[0],
            y: data[1],
            z: data[2],
            w: data[3],
        }
    }

    pub fn point(x: f64, y: f64, z: f64) -> Self {
        Self {
            x: x,
            y: y,
            z: z,
            w: 1.0,
        }
    }

    pub fn point_from_array(data: [f64; 3]) -> Self {
        Self {
            x: data[0],
            y: data[1],
            z: data[2],
            w: 1.0,
        }
    }

    pub fn vector(x: f64, y: f64, z: f64) -> Self {
        Self {
            x: x,
            y: y,
            z: z,
            w: 0.0,
        }
    }

    pub fn vector_from_array(data: [f64; 3]) -> Self {
        Self {
            x: data[0],
            y: data[1],
            z: data[2],
            w: 0.0,
        }
    }

    pub fn is_point(self) -> bool {
        almost_eq(self.w, 1.0)
    }

    pub fn is_vector(self) -> bool {
        self.w == 0.0
    }

    pub fn magnitude(self) -> f64 {
        let m: f64 = self.x.powi(2) + self.y.powi(2) + self.z.powi(2) + self.w.powi(2);
        m.sqrt()
    }

    pub fn normalize(self) -> Self {
        let m = self.magnitude();
        Self {
            x: self.x / m,
            y: self.y / m,
            z: self.z / m,
            w: self.w / m,
        }
    }

    pub fn dot(self, other: Self) -> f64 {
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }

    pub fn cross(self, other: Self) -> Self {
        Self {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.z,
            z: self.x * other.y - self.y * other.x,
            w: self.w,
        }
    }

    pub fn reflect(self, normal: Self) -> Self {
        self - normal * 2.0 * self.dot(normal)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // A tuple with w=1.0 is a point
    #[test]
    fn test_tuple_as_point() {
        let a: Tuple = Tuple::new(4.3, -4.2, 3.1, 1.0);
        assert_eq!(a.x, 4.3);
        assert_eq!(a.y, -4.2);
        assert_eq!(a.z, 3.1);
        assert_eq!(a.w, 1.0);
        assert!(a.is_point());
        assert!(!a.is_vector());
    }

    // A tuple with w=0 is a vector
    #[test]
    fn test_tuple_as_vector() {
        let a: Tuple = Tuple::new(4.3, -4.2, 3.1, 0.0);
        assert_eq!(a.x, 4.3);
        assert_eq!(a.y, -4.2);
        assert_eq!(a.z, 3.1);
        assert_eq!(a.w, 0.0);
        assert!(!a.is_point());
        assert!(a.is_vector());
    }

    // point() creates tuples with w=1
    #[test]
    fn test_point_constructor() {
        let a: Tuple = Tuple::point(4.3, -4.2, 3.1);
        let b: Tuple = Tuple::new(4.3, -4.2, 3.1, 1.0);
        assert_eq!(a, b);
    }

    // vector() creates tuples with w=1
    #[test]
    fn test_vector_constructor() {
        let a: Tuple = Tuple::vector(4.3, -4.2, 3.1);
        let b: Tuple = Tuple::new(4.3, -4.2, 3.1, 0.0);
        assert_eq!(a, b);
    }

    // Adding two tuples
    #[test]
    fn test_add_tuples() {
        let a: Tuple = Tuple::new(3.0, -2.0, 5.0, 1.0);
        let b: Tuple = Tuple::new(-2.0, 3.0, 1.0, 0.0);
        assert_eq!(a + b, Tuple::new(1.0, 1.0, 6.0, 1.0));
    }

    // Subtracting two points
    #[test]
    fn test_sub_points() {
        let a: Tuple = Tuple::point(3.0, 2.0, 1.0);
        let b: Tuple = Tuple::point(5.0, 6.0, 7.0);
        assert_eq!(a - b, Tuple::vector(-2.0, -4.0, -6.0));
    }

    // Subtracting a vector from a point
    #[test]
    fn test_sub_vector_point() {
        let a: Tuple = Tuple::point(3.0, 2.0, 1.0);
        let b: Tuple = Tuple::vector(5.0, 6.0, 7.0);
        assert_eq!(a - b, Tuple::point(-2.0, -4.0, -6.0));
    }

    // Subtracting two vectors
    #[test]
    fn test_sub_vectors() {
        let a: Tuple = Tuple::vector(3.0, 2.0, 1.0);
        let b: Tuple = Tuple::vector(5.0, 6.0, 7.0);
        assert_eq!(a - b, Tuple::vector(-2.0, -4.0, -6.0));
    }

    // Subtracting a vector from the zero vector
    #[test]
    fn test_sub_vector_zero() {
        let zero: Tuple = Tuple::vector(0.0, 0.0, 0.0);
        let v: Tuple = Tuple::vector(1.0, -2.0, 3.0);
        assert_eq!(zero - v, Tuple::vector(-1.0, 2.0, -3.0));
    }

    // Negating a tuple
    #[test]
    fn test_negate_tuple() {
        let a: Tuple = Tuple::new(1.0, -2.0, 3.0, -4.0);
        assert_eq!(-a, Tuple::new(-1.0, 2.0, -3.0, 4.0));
    }

    // Multiplying a tuple by a scalar
    #[test]
    fn test_mul_scalar() {
        let a: Tuple = Tuple::new(1.0, -2.0, 3.0, -4.0);
        assert_eq!(a * 3.5, Tuple::new(3.5, -7.0, 10.5, -14.0));
    }

    // Multiplying a tuple by a fraction
    #[test]
    fn test_mul_fraction() {
        let a: Tuple = Tuple::new(1.0, -2.0, 3.0, -4.0);
        assert_eq!(a * 0.5, Tuple::new(0.5, -1.0, 1.5, -2.0));
    }

    // Dividing a tuple by a scalar
    #[test]
    fn test_div_scalar() {
        let a: Tuple = Tuple::new(1.0, -2.0, 3.0, -4.0);
        assert_eq!(a / 2.0, Tuple::new(0.5, -1.0, 1.5, -2.0));
    }

    // Computing the magnitude of vector(1, 0, 0)
    #[test]
    fn test_magnitude_1() {
        let a: Tuple = Tuple::vector(1.0, 0.0, 0.0);
        assert_eq!(a.magnitude(), 1.0)
    }

    // Computing the magnitude of vector(0, 1, 0)
    #[test]
    fn test_magnitude_2() {
        let a: Tuple = Tuple::vector(0.0, 1.0, 0.0);
        assert_eq!(a.magnitude(), 1.0)
    }

    // Computing the magnitude of vector(0, 0, 1)
    #[test]
    fn test_magnitude_3() {
        let a: Tuple = Tuple::vector(0.0, 0.0, 1.0);
        assert_eq!(a.magnitude(), 1.0)
    }

    // Computing the magnitude of vector(1, 2, 3)
    #[test]
    fn test_magnitude_4() {
        let r: f64 = 14.0;
        let a: Tuple = Tuple::vector(1.0, 2.0, 3.0);
        assert_eq!(a.magnitude(), r.sqrt());
    }

    // Computing the magnitude of vector(-1, -2, -3)
    #[test]
    fn test_magnitude_5() {
        let r: f64 = 14.0;
        let a: Tuple = Tuple::vector(-1.0, -2.0, -3.0);
        assert_eq!(a.magnitude(), r.sqrt());
    }

    // Normalizing vector(4, 0, 0) gives (1, 0, 0)
    #[test]
    fn test_normalize_1() {
        let a: Tuple = Tuple::vector(4.0, 0.0, 0.0);
        assert_eq!(a.normalize(), Tuple::vector(1.0, 0.0, 0.0));
    }

    // Normalizing vector(1, 2, 3)
    #[test]
    fn test_normalize_2() {
        let r: f64 = 14.0;
        let a: Tuple = Tuple::vector(1.0, 2.0, 3.0);
        assert_eq!(
            a.normalize(),
            Tuple::vector(1.0 / r.sqrt(), 2.0 / r.sqrt(), 3.0 / r.sqrt())
        );
    }

    // The magnitude of a normalized vector
    #[test]
    fn test_magnitude_normalize() {
        let a: Tuple = Tuple::vector(1.0, 2.0, 3.0);
        let norm = a.normalize();
        assert!(almost_eq(norm.magnitude(), 1.0));
    }

    // The dot product of two tuples
    #[test]
    fn test_dot_product() {
        let a: Tuple = Tuple::vector(1.0, 2.0, 3.0);
        let b: Tuple = Tuple::vector(2.0, 3.0, 4.0);
        assert_eq!(a.dot(b), 20.0);
    }

    // The cross product of two vectors
    #[test]
    fn test_cross_product() {
        let a: Tuple = Tuple::vector(1.0, 2.0, 3.0);
        let b: Tuple = Tuple::vector(2.0, 3.0, 4.0);
        assert_eq!(a.cross(b), Tuple::vector(-1.0, 2.0, -1.0));
        assert_eq!(b.cross(a), Tuple::vector(1.0, -2.0, 1.0));
    }

    // Reflecting a vector approaching at 45°
    #[test]
    fn test_reflect_45() {
        let v = Tuple::vector(1.0, -1.0, 0.0);
        let n = Tuple::vector(0.0, 1.0, 0.0);
        let r = v.reflect(n);
        assert_eq!(r, Tuple::vector(1.0, 1.0, 0.0));
    }

    // Reflecting a vector off a slanted surface
    #[test]
    fn test_reflect_slanted() {
        let v = Tuple::vector(0.0, -1.0, 0.0);
        let x = (2.0 as f64).sqrt() / 2.0;
        let n = Tuple::vector(x, x, 0.0);
        let r = v.reflect(n);
        assert_eq!(r, Tuple::vector(1.0, 0.0, 0.0));
    }
}
