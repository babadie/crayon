use crate::ray::{Hittable, Intersection, Intersections, Ray};
use crate::shapes::{Object, Shape};
use crate::tuple::Tuple;
use crate::EPSILON;
use serde::Deserialize;
use std::convert::From;

#[derive(Debug, Copy, Clone, Deserialize)]
pub struct Plane {
    #[serde(flatten)]
    pub shape: Shape,
}

impl Plane {
    pub fn new() -> Self {
        Self {
            shape: Shape::new(),
        }
    }
}

impl PartialEq for Plane {
    fn eq(&self, other: &Self) -> bool {
        return self.shape == other.shape;
    }
}

impl From<Plane> for Object {
    fn from(sphere: Plane) -> Self {
        Object::Plane(sphere)
    }
}

impl Hittable for Plane {
    fn normal_at(&self, _world_point: Tuple) -> Tuple {
        // The object normal in local coordinates for an XZ plane is
        // always the Vector (0, 1, 0)
        let obj_normal = Tuple::vector(0.0, 1.0, 0.0);

        // Output the object normal converted into world coordinates
        self.shape.calc_world_normal(obj_normal)
    }

    fn intersect(self, ray: Ray) -> Intersections {
        let local_ray = self.shape.calc_local_ray(ray);

        // The plane is always defined in XZ axis, so if ray does not
        // intersect it at all when it's on the Y axis (as it would be parallel)
        if local_ray.direction.y.abs() < EPSILON {
            return vec![];
        }

        // Calculate intersection times
        let t = Intersection::new(
            -local_ray.origin.y / local_ray.direction.y,
            Object::Plane(self),
        );
        return vec![t];
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // The normal of a plane is constant everywhere
    #[test]
    fn test_normal() {
        let p = Plane::new();
        assert_eq!(
            p.normal_at(Tuple::point(0.0, 0.0, 0.0)),
            Tuple::vector(0.0, 1.0, 0.0)
        );
        assert_eq!(
            p.normal_at(Tuple::point(10.0, 0.0, -10.0)),
            Tuple::vector(0.0, 1.0, 0.0)
        );
        assert_eq!(
            p.normal_at(Tuple::point(-5.0, 0.0, 150.0)),
            Tuple::vector(0.0, 1.0, 0.0)
        );
    }

    // Intersect with a ray parallel to the plane
    #[test]
    fn test_intersect_parallel() {
        let p = Plane::new();
        let r = Ray::new(Tuple::point(0.0, 10.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        let xs = p.intersect(r);
        assert_eq!(xs.len(), 0);
    }

    // Intersect with a coplanara ray
    #[test]
    fn test_intersect_coplanar() {
        let p = Plane::new();
        let r = Ray::new(Tuple::point(0.0, 0.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        let xs = p.intersect(r);
        assert_eq!(xs.len(), 0);
    }

    // A ray intersecting a plane from above
    #[test]
    fn test_intersect_above() {
        let p = Plane::new();
        let r = Ray::new(Tuple::point(0.0, 1.0, 0.0), Tuple::vector(0.0, -1.0, 0.0));
        let xs = p.intersect(r);
        assert_eq!(xs.len(), 1);
        assert_eq!(xs[0].time, 1.0);
        assert_eq!(xs[0].object, Object::from(p));
    }

    // A ray intersecting a plane from below
    #[test]
    fn test_intersect_below() {
        let p = Plane::new();
        let r = Ray::new(Tuple::point(0.0, -1.0, 0.0), Tuple::vector(0.0, 1.0, 0.0));
        let xs = p.intersect(r);
        assert_eq!(xs.len(), 1);
        assert_eq!(xs[0].time, 1.0);
        assert_eq!(xs[0].object, Object::from(p));
    }
}
