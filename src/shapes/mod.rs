pub mod plane;
pub mod sphere;

use crate::light::Material;
use crate::matrix::Matrix;
use crate::ray::Ray;
use crate::shapes::plane::Plane;
use crate::shapes::sphere::Sphere;
use crate::tuple::Tuple;

use serde::Deserialize;

// Common informations for all 3D primitives
#[derive(Debug, Copy, Clone, Deserialize)]
pub struct Shape {
    pub transform: Matrix,

    #[serde(default)]
    pub material: Material,
}

impl Shape {
    pub fn new() -> Self {
        Self {
            transform: Matrix::identity(4),
            material: Material::default(),
        }
    }

    // Helper for intersect() on 3D primitives to get the world ray
    // transformed into local object coordinates
    pub fn calc_local_ray(self, ray: Ray) -> Ray {
        ray.transform(self.transform.inverse())
    }

    // Helper for normal_at() on 3D primitives to get the world point
    // transformed into local object coordinates
    pub fn calc_local_point(self, point: Tuple) -> Tuple {
        assert!(point.is_point());
        self.transform.inverse() * point
    }

    // Helper for normal_at() on 3D primitives to get the local normal
    // on the primitive transformed into world coordinates
    pub fn calc_world_normal(self, normal: Tuple) -> Tuple {
        assert!(normal.is_vector());

        // Now we can have the world normal, by preserving the original angles
        // through normalization
        let world_normal = self.transform.inverse().transpose() * normal;

        // But its w coord can be messed up, so we hardcode it to 0
        Tuple::new(world_normal.x, world_normal.y, world_normal.z, 0.0).normalize()
    }
}

impl Default for Shape {
    fn default() -> Self {
        Self::new()
    }
}

impl PartialEq for Shape {
    fn eq(&self, other: &Self) -> bool {
        return self.transform == other.transform && self.material == other.material;
    }
}

// The list of all supported 3D primitives in the Ray Tracer
// Each of those must implement Hittable and have a Shape
#[derive(Debug, Copy, Clone, Deserialize)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum Object {
    Sphere(Sphere),
    Plane(Plane),
}

impl PartialEq for Object {
    fn eq(&self, other: &Self) -> bool {
        return match (self, other) {
            (Object::Sphere(x), Object::Sphere(y)) => x == y,
            (Object::Plane(x), Object::Plane(y)) => x == y,

            // When types do not match, cannot compare
            _ => false,
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // A shape's default transformation
    #[test]
    fn test_shape_transformation() {
        let s = Shape::new();
        assert_eq!(s.transform, Matrix::identity(4));
    }

    // Changing a shape's transformation
    #[test]
    fn test_shape_transformation_update() {
        let mut s = Shape::new();
        s.transform = Matrix::translation(2.0, 3.0, 4.0);
        assert_eq!(s.transform, Matrix::translation(2.0, 3.0, 4.0));
    }

    // A shape has a default material
    #[test]
    fn test_material() {
        let s = Shape::new();
        assert_eq!(s.material, Material::default());
    }

    // A shape can be assigned a material
    #[test]
    fn test_material_update() {
        let mut s = Shape::new();
        let mut m = Material::default();
        m.ambient = 1.0;
        s.material = m.clone();
        assert_eq!(s.material, m);
    }
}
