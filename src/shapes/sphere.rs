use crate::parser::deserialize_point;
use crate::ray::{Hittable, Intersection, Intersections, Ray};
use crate::shapes::{Object, Shape};
use crate::tuple::Tuple;
use serde::Deserialize;
use std::convert::From;
use std::f64;

fn default_origin() -> Tuple {
    Tuple::point(0.0, 0.0, 0.0)
}

fn default_radius() -> f64 {
    1.0
}

#[derive(Debug, Copy, Clone, Deserialize)]
pub struct Sphere {
    #[serde(flatten)]
    pub shape: Shape,

    #[serde(deserialize_with = "deserialize_point")]
    #[serde(default = "default_origin")]
    pub origin: Tuple,

    #[serde(default = "default_radius")]
    pub radius: f64,
}

impl Sphere {
    fn new(origin: Tuple, radius: f64) -> Self {
        assert!(origin.is_point());
        Self {
            shape: Shape::new(),
            origin: origin,
            radius: radius,
        }
    }

    pub fn simple() -> Self {
        // Build a simple sphere at world's origin
        // of radii 1
        Self::new(default_origin(), default_radius())
    }
}

impl PartialEq for Sphere {
    fn eq(&self, other: &Self) -> bool {
        return self.origin == other.origin
            && self.radius == other.radius
            && self.shape == other.shape;
    }
}

impl From<Sphere> for Object {
    fn from(sphere: Sphere) -> Self {
        Object::Sphere(sphere)
    }
}

impl Hittable for Sphere {
    fn normal_at(&self, world_point: Tuple) -> Tuple {
        // In object space, the object origin, is the origin of the "world" !
        let obj_normal = self.shape.calc_local_point(world_point) - self.origin;

        // Output the object normal converted into world coordinated
        self.shape.calc_world_normal(obj_normal)
    }

    fn intersect(self, ray: Ray) -> Intersections {
        let ray_transform = self.shape.calc_local_ray(ray);

        let sphere_to_ray = ray_transform.origin - self.origin;
        let a = ray_transform.direction.dot(ray_transform.direction);
        let b = 2.0 * ray_transform.direction.dot(sphere_to_ray);
        let c = sphere_to_ray.dot(sphere_to_ray) - self.radius;

        let discriminant = b.powi(2) - 4.0 * a * c;

        // No intersection
        if discriminant < 0.0 {
            return vec![];
        }

        // Calculate intersection times
        let t1 = Intersection::new((-b - discriminant.sqrt()) / (2.0 * a), Object::Sphere(self));
        let t2 = Intersection::new((-b + discriminant.sqrt()) / (2.0 * a), Object::Sphere(self));
        return vec![t1, t2];
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::matrix::Matrix;

    // The normal on a sphere at a point on the X axis
    #[test]
    fn test_normal_x() {
        let s = Sphere::simple();
        let n = s.normal_at(Tuple::point(1.0, 0.0, 0.0));
        assert_eq!(n, Tuple::vector(1.0, 0.0, 0.0));
    }

    // The normal on a sphere at a point on the Y axis
    #[test]
    fn test_normal_y() {
        let s = Sphere::simple();
        let n = s.normal_at(Tuple::point(0.0, 1.0, 0.0));
        assert_eq!(n, Tuple::vector(0.0, 1.0, 0.0));
    }

    // The normal on a sphere at a point on the X axis
    #[test]
    fn test_normal_z() {
        let s = Sphere::simple();
        let n = s.normal_at(Tuple::point(0.0, 1.0, 0.0));
        assert_eq!(n, Tuple::vector(0.0, 1.0, 0.0));
    }

    // The normal on a sphere at a nonaxial point
    #[test]
    fn test_normal_nonaxial() {
        let s = Sphere::simple();
        let x = (3.0 as f64).sqrt() / 3.0;
        let n = s.normal_at(Tuple::point(x, x, x));
        assert_eq!(n, Tuple::vector(x, x, x));
    }

    // The normal is a normalized vector
    #[test]
    fn test_normal_normalized() {
        let s = Sphere::simple();
        let x = (3.0 as f64).sqrt() / 3.0;
        let n = s.normal_at(Tuple::point(x, x, x));
        assert_eq!(n, n.normalize());
    }

    // Computing the normal on a translated sphere
    #[test]
    fn test_normal_translated() {
        let mut s = Sphere::simple();
        s.shape.transform = Matrix::translation(0.0, 1.0, 0.0);
        let n = s.normal_at(Tuple::point(0.0, 1.70711, -0.70711));
        assert_eq!(n, Tuple::vector(0.0, 0.70711, -0.70711));
    }

    // Computing the normal on a transformed sphere
    #[test]
    fn test_normal_transformed() {
        let mut s = Sphere::simple();
        s.shape.transform =
            Matrix::scaling(1.0, 0.5, 1.0) * Matrix::rotation_z(f64::consts::PI / 5.0);
        let x = (2.0 as f64).sqrt() / 2.0;
        let n = s.normal_at(Tuple::point(0.0, x, -x));
        assert_eq!(n, Tuple::vector(0.0, 0.97014, -0.24254));
    }
}
