use clap::Parser;
use crayon::parser::parse_scene;
use std::time::Instant;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Path towards a scene description in YAML
    #[clap(short, long)]
    scene: String,

    /// Output path of the rendered scene in PPM
    #[clap(short, long, default_value = "out.ppm")]
    output: String,

    /// Display verbose logs
    #[clap(short, long)]
    verbose: bool,

    /// Rendered image width, defaults to values from scene
    #[clap(short, long)]
    width: Option<usize>,

    /// Rendered image height, defaults to values from scene
    #[clap(short, long)]
    height: Option<usize>,
}

fn main() {
    // Parse CLI args
    let args = Cli::parse();

    // Parse the provided scene
    let (mut camera, world) = parse_scene(args.scene.as_str());

    // Update camera width and height
    match (args.width, args.height) {
        (Some(width), Some(height)) => camera.set_size(width, height),
        (Some(_), None) => {
            println!("Missing height !");
        }
        (None, Some(_)) => {
            println!("Missing width !");
        }
        _ => {}
    }

    // Debug parsed camera & world
    if args.verbose {
        println!("Camera from {} {:#?}", args.scene, camera);
        println!("World from {} {:#?}", args.scene, world);
    }

    // Start timer to get rendering time
    let now = Instant::now();

    // Use camera to render
    let canvas = camera.render(world);
    canvas.save(args.output.as_str()).expect("oups");

    println!(
        "Rendered {} into {} in {} ms",
        args.scene,
        args.output,
        now.elapsed().as_millis()
    );
}
